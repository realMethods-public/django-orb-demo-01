"""mainsite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
urlpatterns = [
    path('User/', include('djangodemo.urls.UserUrls')),
    path('Referrer/', include('djangodemo.urls.ReferrerUrls')),
    path('TheReference/', include('djangodemo.urls.TheReferenceUrls')),
    path('ReferrerGroup/', include('djangodemo.urls.ReferrerGroupUrls')),
    path('ReferenceEngine/', include('djangodemo.urls.ReferenceEngineUrls')),
    path('Question/', include('djangodemo.urls.QuestionUrls')),
    path('TheResponse/', include('djangodemo.urls.TheResponseUrls')),
    path('QuestionGroup/', include('djangodemo.urls.QuestionGroupUrls')),
    path('Admin/', include('djangodemo.urls.AdminUrls')),
    path('Activity/', include('djangodemo.urls.ActivityUrls')),
    path('Comment/', include('djangodemo.urls.CommentUrls')),
    path('Answer/', include('djangodemo.urls.AnswerUrls')),
    path('ReferenceGroupLink/', include('djangodemo.urls.ReferenceGroupLinkUrls')),
    path('admin/', admin.site.urls),
]