from django.contrib import admin

# Register your models here.
from .models.User import User
from .models.Referrer import Referrer
from .models.TheReference import TheReference
from .models.ReferrerGroup import ReferrerGroup
from .models.ReferenceEngine import ReferenceEngine
from .models.Question import Question
from .models.TheResponse import TheResponse
from .models.QuestionGroup import QuestionGroup
from .models.Admin import Admin
from .models.Activity import Activity
from .models.Comment import Comment
from .models.Answer import Answer
from .models.ReferenceGroupLink import ReferenceGroupLink

# Need to add this for each model that requires managing

admin.site.register(User)
admin.site.register(Referrer)
admin.site.register(TheReference)
admin.site.register(ReferrerGroup)
admin.site.register(ReferenceEngine)
admin.site.register(Question)
admin.site.register(TheResponse)
admin.site.register(QuestionGroup)
admin.site.register(Admin)
admin.site.register(Activity)
admin.site.register(Comment)
admin.site.register(Answer)
admin.site.register(ReferenceGroupLink)
