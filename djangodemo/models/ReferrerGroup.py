from django.db import models
#======================================================================
# 
# Encapsulates data for model ReferrerGroup
#
# @author 
#
#======================================================================

#======================================================================
# Class ReferrerGroup Declaration
#======================================================================
class ReferrerGroup (models.Model):

#======================================================================
# attribute declarations
#======================================================================
	name = models.CharField(max_length=200, null=True)
	dateTimeLastViewedExternally = models.CharField(max_length=64, null=True)
	references = models.ManyToManyField('TheReference',  blank=True, related_name='+')

#======================================================================
# function declarations
#======================================================================
	def toString(self):
		str = ""
		str = str + self.name
		str = str + self.dateTimeLastViewedExternally
		return str;
    
	def __str__(self):
		return self.toString();

	def identity(self):
		return "ReferrerGroup";
    
	def objectType(self):
		return "ReferrerGroup";
