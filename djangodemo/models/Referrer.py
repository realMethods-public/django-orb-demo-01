from django.db import models
#======================================================================
# 
# Encapsulates data for model Referrer
#
# @author 
#
#======================================================================

#======================================================================
# Class Referrer Declaration
#======================================================================
class Referrer (models.Model):

#======================================================================
# attribute declarations
#======================================================================
	firstName = models.CharField(max_length=200, null=True)
	lastName = models.CharField(max_length=200, null=True)
	emailAddress = models.CharField(max_length=200, null=True)
	active = models.BooleanField(null=True)
	comments = models.ManyToManyField('Comment',  blank=True, related_name='+')

#======================================================================
# function declarations
#======================================================================
	def toString(self):
		str = ""
		str = str + self.firstName
		str = str + self.lastName
		str = str + self.emailAddress
		str = str + self.active
		return str;
    
	def __str__(self):
		return self.toString();

	def identity(self):
		return "Referrer";
    
	def objectType(self):
		return "Referrer";
