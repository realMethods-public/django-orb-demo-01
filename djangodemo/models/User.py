from django.db import models
from .Referrer import Referrer
#======================================================================
# 
# Encapsulates data for model User
#
# @author 
#
#======================================================================

#======================================================================
# Class User Declaration
#======================================================================
class User (Referrer):

#======================================================================
# attribute declarations
#======================================================================
	password = models.CharField(max_length=200, null=True)
	resumeLinkUrl = models.CharField(max_length=200, null=True)
	linkedInUrl = models.CharField(max_length=200, null=True)
	referenceProviders = models.ManyToManyField('Referrer',  blank=True, related_name='+')
	refererGroups = models.ManyToManyField('ReferrerGroup',  blank=True, related_name='+')
	referenceReceivers = models.ManyToManyField('Referrer',  blank=True, related_name='+')
	referenceGroupLinks = models.ManyToManyField('ReferenceGroupLink',  blank=True, related_name='+')

#======================================================================
# function declarations
#======================================================================
	def toString(self):
		str = ""
		str = str + self.password
		str = str + self.resumeLinkUrl
		str = str + self.linkedInUrl
		return str;
    
	def __str__(self):
		return self.toString();

	def identity(self):
		return "User";
    
	def objectType(self):
		return "User";
