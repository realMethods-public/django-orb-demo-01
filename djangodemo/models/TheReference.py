from django.db import models
#======================================================================
# 
# Encapsulates data for model TheReference
#
# @author 
#
#======================================================================

#======================================================================
# Class TheReference Declaration
#======================================================================
class TheReference (models.Model):

#======================================================================
# attribute declarations
#======================================================================
	dateLastUpdated = models.CharField(max_length=64, null=True)
	active = models.BooleanField(null=True)
	dateTimeLastViewedExternally = models.CharField(max_length=64, null=True)
	makeViewableToUser = models.BooleanField(null=True)
	dateLastSent = models.CharField(max_length=64, null=True)
	numDaysToExpire = models.IntegerField(null=True)
	questionGroup = models.OneToOneField('QuestionGroup', on_delete=models.CASCADE, null=True, blank=True, related_name='+')
	status = models.CharField(max_length=64, null=True)
	type = models.CharField(max_length=64, null=True)
	user = models.OneToOneField('User', on_delete=models.CASCADE, null=True, blank=True, related_name='+')
	referrer = models.OneToOneField('Referrer', on_delete=models.CASCADE, null=True, blank=True, related_name='+')
	answers = models.ManyToManyField('Answer',  blank=True, related_name='+')
	lastQuestionAnswered = models.OneToOneField('Question', on_delete=models.CASCADE, null=True, blank=True, related_name='+')

#======================================================================
# function declarations
#======================================================================
	def toString(self):
		str = ""
		str = str + self.dateLastUpdated
		str = str + self.active
		str = str + self.dateTimeLastViewedExternally
		str = str + self.makeViewableToUser
		str = str + self.dateLastSent
		str = str + self.numDaysToExpire
		str = str + self.status
		str = str + self.type
		return str;
    
	def __str__(self):
		return self.toString();

	def identity(self):
		return "TheReference";
    
	def objectType(self):
		return "TheReference";
