from django.db import models
#======================================================================
# 
# Encapsulates data for model ReferenceGroupLink
#
# @author 
#
#======================================================================

#======================================================================
# Class ReferenceGroupLink Declaration
#======================================================================
class ReferenceGroupLink (models.Model):

#======================================================================
# attribute declarations
#======================================================================
	dateLinkCreated = models.CharField(max_length=64, null=True)
	name = models.CharField(max_length=200, null=True)
	referrerGroup = models.OneToOneField('ReferrerGroup', on_delete=models.CASCADE, null=True, blank=True, related_name='+')
	linkProvider = models.OneToOneField('User', on_delete=models.CASCADE, null=True, blank=True, related_name='+')

#======================================================================
# function declarations
#======================================================================
	def toString(self):
		str = ""
		str = str + self.dateLinkCreated
		str = str + self.name
		return str;
    
	def __str__(self):
		return self.toString();

	def identity(self):
		return "ReferenceGroupLink";
    
	def objectType(self):
		return "ReferenceGroupLink";
