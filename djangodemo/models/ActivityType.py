from django.db import models
 #======================================================================
# 
# Encapsulates data for model ActivityType
#
# @author 
#
#======================================================================

#======================================================================
# Class ActivityType Declaration (enumerated type)
#======================================================================
#getClassDecl( $class "")
	referenceStarted = 'referenceStarted'
	referenceNotificationExpired = 'referenceNotificationExpired'
	referenceCompleted = 'referenceCompleted'
	referenceGroupChecked = 'referenceGroupChecked'
	commentCreated = 'commentCreated'
	referenceMadePublic = 'referenceMadePublic'
	referenceMadePrivate = 'referenceMadePrivate'
