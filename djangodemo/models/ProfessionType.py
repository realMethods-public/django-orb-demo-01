from django.db import models
 #======================================================================
# 
# Encapsulates data for model ProfessionType
#
# @author 
#
#======================================================================

#======================================================================
# Class ProfessionType Declaration (enumerated type)
#======================================================================
#getClassDecl( $class "")
	academic = 'academic'
	technical = 'technical'
	medical = 'medical'
	construction = 'construction'
	politics = 'politics'
	professional = 'professional'
	layworker = 'layworker'
	legal = 'legal'
	social = 'social'
