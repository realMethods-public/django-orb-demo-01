from django.db import models
#======================================================================
# 
# Encapsulates data for model Comment
#
# @author 
#
#======================================================================

#======================================================================
# Class Comment Declaration
#======================================================================
class Comment (models.Model):

#======================================================================
# attribute declarations
#======================================================================
	commentText = models.CharField(max_length=200, null=True)
	source = models.OneToOneField('Referrer', on_delete=models.CASCADE, null=True, blank=True, related_name='+')

#======================================================================
# function declarations
#======================================================================
	def toString(self):
		str = ""
		str = str + self.commentText
		return str;
    
	def __str__(self):
		return self.toString();

	def identity(self):
		return "Comment";
    
	def objectType(self):
		return "Comment";
