from django.db import models
#======================================================================
# 
# Encapsulates data for model Answer
#
# @author 
#
#======================================================================

#======================================================================
# Class Answer Declaration
#======================================================================
class Answer (models.Model):

#======================================================================
# attribute declarations
#======================================================================
	question = models.OneToOneField('Question', on_delete=models.CASCADE, null=True, blank=True, related_name='+')
	response = models.OneToOneField('TheResponse', on_delete=models.CASCADE, null=True, blank=True, related_name='+')

#======================================================================
# function declarations
#======================================================================
	def toString(self):
		str = ""
		return str;
    
	def __str__(self):
		return self.toString();

	def identity(self):
		return "Answer";
    
	def objectType(self):
		return "Answer";
