from django.db import models
#======================================================================
# 
# Encapsulates data for model Admin
#
# @author 
#
#======================================================================

#======================================================================
# Class Admin Declaration
#======================================================================
class Admin (models.Model):

#======================================================================
# attribute declarations
#======================================================================
	loginId = models.CharField(max_length=200, null=True)
	password = models.CharField(max_length=200, null=True)
	users = models.ManyToManyField('User',  blank=True, related_name='+')
	referenceEngines = models.ManyToManyField('ReferenceEngine',  blank=True, related_name='+')

#======================================================================
# function declarations
#======================================================================
	def toString(self):
		str = ""
		str = str + self.loginId
		str = str + self.password
		return str;
    
	def __str__(self):
		return self.toString();

	def identity(self):
		return "Admin";
    
	def objectType(self):
		return "Admin";
