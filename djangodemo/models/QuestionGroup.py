from django.db import models
#======================================================================
# 
# Encapsulates data for model QuestionGroup
#
# @author 
#
#======================================================================

#======================================================================
# Class QuestionGroup Declaration
#======================================================================
class QuestionGroup (models.Model):

#======================================================================
# attribute declarations
#======================================================================
	aggregateScore = models.CharField(max_length=64, null=True)
	weight = models.CharField(max_length=64, null=True)
	name = models.CharField(max_length=200, null=True)
	questions = models.ManyToManyField('Question',  blank=True, related_name='+')
	questionGroups = models.ManyToManyField('QuestionGroup',  blank=True, related_name='+')

#======================================================================
# function declarations
#======================================================================
	def toString(self):
		str = ""
		str = str + self.aggregateScore
		str = str + self.weight
		str = str + self.name
		return str;
    
	def __str__(self):
		return self.toString();

	def identity(self):
		return "QuestionGroup";
    
	def objectType(self):
		return "QuestionGroup";
