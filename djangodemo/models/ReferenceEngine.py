from django.db import models
#======================================================================
# 
# Encapsulates data for model ReferenceEngine
#
# @author 
#
#======================================================================

#======================================================================
# Class ReferenceEngine Declaration
#======================================================================
class ReferenceEngine (models.Model):

#======================================================================
# attribute declarations
#======================================================================
	name = models.CharField(max_length=200, null=True)
	active = models.BooleanField(null=True)
	mainQuestionGroup = models.OneToOneField('QuestionGroup', on_delete=models.CASCADE, null=True, blank=True, related_name='+')
	purpose = models.CharField(max_length=64, null=True)

#======================================================================
# function declarations
#======================================================================
	def toString(self):
		str = ""
		str = str + self.name
		str = str + self.active
		str = str + self.purpose
		return str;
    
	def __str__(self):
		return self.toString();

	def identity(self):
		return "ReferenceEngine";
    
	def objectType(self):
		return "ReferenceEngine";
