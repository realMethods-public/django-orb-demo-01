from django.db import models
#======================================================================
# 
# Encapsulates data for model Question
#
# @author 
#
#======================================================================

#======================================================================
# Class Question Declaration
#======================================================================
class Question (models.Model):

#======================================================================
# attribute declarations
#======================================================================
	weight = models.CharField(max_length=64, null=True)
	questionText = models.CharField(max_length=200, null=True)
	identifier = models.CharField(max_length=200, null=True)
	mustBeAnswered = models.BooleanField(null=True)
	responseExclusive = models.BooleanField(null=True)
	responses = models.ManyToManyField('TheResponse',  blank=True, related_name='+')

#======================================================================
# function declarations
#======================================================================
	def toString(self):
		str = ""
		str = str + self.weight
		str = str + self.questionText
		str = str + self.identifier
		str = str + self.mustBeAnswered
		str = str + self.responseExclusive
		return str;
    
	def __str__(self):
		return self.toString();

	def identity(self):
		return "Question";
    
	def objectType(self):
		return "Question";
