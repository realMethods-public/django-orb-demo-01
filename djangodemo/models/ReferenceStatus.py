from django.db import models
 #======================================================================
# 
# Encapsulates data for model ReferenceStatus
#
# @author 
#
#======================================================================

#======================================================================
# Class ReferenceStatus Declaration (enumerated type)
#======================================================================
#getClassDecl( $class "")
	notYetStarted = 'notYetStarted'
	inProgress = 'inProgress'
	completed = 'completed'
