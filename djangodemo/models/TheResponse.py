from django.db import models
#======================================================================
# 
# Encapsulates data for model TheResponse
#
# @author 
#
#======================================================================

#======================================================================
# Class TheResponse Declaration
#======================================================================
class TheResponse (models.Model):

#======================================================================
# attribute declarations
#======================================================================
	responseText = models.CharField(max_length=200, null=True)
	gotoQuestionId = models.CharField(max_length=200, null=True)

#======================================================================
# function declarations
#======================================================================
	def toString(self):
		str = ""
		str = str + self.responseText
		str = str + self.gotoQuestionId
		return str;
    
	def __str__(self):
		return self.toString();

	def identity(self):
		return "TheResponse";
    
	def objectType(self):
		return "TheResponse";
