from django.db import models
#======================================================================
# 
# Encapsulates data for model Activity
#
# @author 
#
#======================================================================

#======================================================================
# Class Activity Declaration
#======================================================================
class Activity (models.Model):

#======================================================================
# attribute declarations
#======================================================================
	refObjId = models.CharField(max_length=64, null=True)
	createDateTime = models.CharField(max_length=64, null=True)
	type = models.CharField(max_length=64, null=True)
	user = models.OneToOneField('User', on_delete=models.CASCADE, null=True, blank=True, related_name='+')

#======================================================================
# function declarations
#======================================================================
	def toString(self):
		str = ""
		str = str + self.refObjId
		str = str + self.createDateTime
		str = str + self.type
		return str;
    
	def __str__(self):
		return self.toString();

	def identity(self):
		return "Activity";
    
	def objectType(self):
		return "Activity";
