from django.urls import path
from djangodemo.views import QuestionView

urlpatterns = [
    path('', QuestionView.index, name='index'),
	path('create', QuestionView.get, name='create'),
	path('get/<int:questionId>/', QuestionView.get, name='get'),
	path('save', QuestionView.save, name='save'),
	path('getAll', QuestionView.getAll, name='getAll'),
	path('delete/<int:questionId>/', QuestionView.delete, name='delete'),
	path('addResponses/<int:questionId>/<ResponsesIds>/', QuestionView.addResponses, name='addResponses'),
	path('removeResponses/<int:questionId>/<ResponsesIds>/', QuestionView.removeResponses, name='removeResponses'),
]
