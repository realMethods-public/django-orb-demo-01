from django.urls import path
from djangodemo.views import UserView

urlpatterns = [
    path('', UserView.index, name='index'),
	path('create', UserView.get, name='create'),
	path('get/<int:userId>/', UserView.get, name='get'),
	path('save', UserView.save, name='save'),
	path('getAll', UserView.getAll, name='getAll'),
	path('delete/<int:userId>/', UserView.delete, name='delete'),
	path('addReferenceProviders/<int:userId>/<ReferenceProvidersIds>/', UserView.addReferenceProviders, name='addReferenceProviders'),
	path('removeReferenceProviders/<int:userId>/<ReferenceProvidersIds>/', UserView.removeReferenceProviders, name='removeReferenceProviders'),
	path('addRefererGroups/<int:userId>/<RefererGroupsIds>/', UserView.addRefererGroups, name='addRefererGroups'),
	path('removeRefererGroups/<int:userId>/<RefererGroupsIds>/', UserView.removeRefererGroups, name='removeRefererGroups'),
	path('addReferenceReceivers/<int:userId>/<ReferenceReceiversIds>/', UserView.addReferenceReceivers, name='addReferenceReceivers'),
	path('removeReferenceReceivers/<int:userId>/<ReferenceReceiversIds>/', UserView.removeReferenceReceivers, name='removeReferenceReceivers'),
	path('addReferenceGroupLinks/<int:userId>/<ReferenceGroupLinksIds>/', UserView.addReferenceGroupLinks, name='addReferenceGroupLinks'),
	path('removeReferenceGroupLinks/<int:userId>/<ReferenceGroupLinksIds>/', UserView.removeReferenceGroupLinks, name='removeReferenceGroupLinks'),
]
