from django.urls import path
from djangodemo.views import ReferenceEngineView

urlpatterns = [
    path('', ReferenceEngineView.index, name='index'),
	path('create', ReferenceEngineView.get, name='create'),
	path('get/<int:referenceEngineId>/', ReferenceEngineView.get, name='get'),
	path('save', ReferenceEngineView.save, name='save'),
	path('getAll', ReferenceEngineView.getAll, name='getAll'),
	path('delete/<int:referenceEngineId>/', ReferenceEngineView.delete, name='delete'),
	path('assignMainQuestionGroup/<int:referenceEngineId>/<int:MainQuestionGroupId>/', ReferenceEngineView.assignMainQuestionGroup, name='assignMainQuestionGroup'),
	path('unassignMainQuestionGroup/<int:referenceEngineId>/', ReferenceEngineView.unassignMainQuestionGroup, name='unassignMainQuestionGroup'),
]
