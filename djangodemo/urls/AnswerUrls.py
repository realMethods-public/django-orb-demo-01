from django.urls import path
from djangodemo.views import AnswerView

urlpatterns = [
    path('', AnswerView.index, name='index'),
	path('create', AnswerView.get, name='create'),
	path('get/<int:answerId>/', AnswerView.get, name='get'),
	path('save', AnswerView.save, name='save'),
	path('getAll', AnswerView.getAll, name='getAll'),
	path('delete/<int:answerId>/', AnswerView.delete, name='delete'),
	path('assignQuestion/<int:answerId>/<int:QuestionId>/', AnswerView.assignQuestion, name='assignQuestion'),
	path('unassignQuestion/<int:answerId>/', AnswerView.unassignQuestion, name='unassignQuestion'),
	path('assignResponse/<int:answerId>/<int:ResponseId>/', AnswerView.assignResponse, name='assignResponse'),
	path('unassignResponse/<int:answerId>/', AnswerView.unassignResponse, name='unassignResponse'),
]
