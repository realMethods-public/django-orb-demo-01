from django.urls import path
from djangodemo.views import ActivityView

urlpatterns = [
    path('', ActivityView.index, name='index'),
	path('create', ActivityView.get, name='create'),
	path('get/<int:activityId>/', ActivityView.get, name='get'),
	path('save', ActivityView.save, name='save'),
	path('getAll', ActivityView.getAll, name='getAll'),
	path('delete/<int:activityId>/', ActivityView.delete, name='delete'),
	path('assignUser/<int:activityId>/<int:UserId>/', ActivityView.assignUser, name='assignUser'),
	path('unassignUser/<int:activityId>/', ActivityView.unassignUser, name='unassignUser'),
]
