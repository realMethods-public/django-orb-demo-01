from django.urls import path
from djangodemo.views import TheResponseView

urlpatterns = [
    path('', TheResponseView.index, name='index'),
	path('create', TheResponseView.get, name='create'),
	path('get/<int:theResponseId>/', TheResponseView.get, name='get'),
	path('save', TheResponseView.save, name='save'),
	path('getAll', TheResponseView.getAll, name='getAll'),
	path('delete/<int:theResponseId>/', TheResponseView.delete, name='delete'),
]
