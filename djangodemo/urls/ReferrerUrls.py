from django.urls import path
from djangodemo.views import ReferrerView

urlpatterns = [
    path('', ReferrerView.index, name='index'),
	path('create', ReferrerView.get, name='create'),
	path('get/<int:referrerId>/', ReferrerView.get, name='get'),
	path('save', ReferrerView.save, name='save'),
	path('getAll', ReferrerView.getAll, name='getAll'),
	path('delete/<int:referrerId>/', ReferrerView.delete, name='delete'),
	path('addComments/<int:referrerId>/<CommentsIds>/', ReferrerView.addComments, name='addComments'),
	path('removeComments/<int:referrerId>/<CommentsIds>/', ReferrerView.removeComments, name='removeComments'),
]
