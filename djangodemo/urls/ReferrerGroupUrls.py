from django.urls import path
from djangodemo.views import ReferrerGroupView

urlpatterns = [
    path('', ReferrerGroupView.index, name='index'),
	path('create', ReferrerGroupView.get, name='create'),
	path('get/<int:referrerGroupId>/', ReferrerGroupView.get, name='get'),
	path('save', ReferrerGroupView.save, name='save'),
	path('getAll', ReferrerGroupView.getAll, name='getAll'),
	path('delete/<int:referrerGroupId>/', ReferrerGroupView.delete, name='delete'),
	path('addReferences/<int:referrerGroupId>/<ReferencesIds>/', ReferrerGroupView.addReferences, name='addReferences'),
	path('removeReferences/<int:referrerGroupId>/<ReferencesIds>/', ReferrerGroupView.removeReferences, name='removeReferences'),
]
