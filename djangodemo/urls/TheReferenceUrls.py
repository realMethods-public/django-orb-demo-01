from django.urls import path
from djangodemo.views import TheReferenceView

urlpatterns = [
    path('', TheReferenceView.index, name='index'),
	path('create', TheReferenceView.get, name='create'),
	path('get/<int:theReferenceId>/', TheReferenceView.get, name='get'),
	path('save', TheReferenceView.save, name='save'),
	path('getAll', TheReferenceView.getAll, name='getAll'),
	path('delete/<int:theReferenceId>/', TheReferenceView.delete, name='delete'),
	path('assignQuestionGroup/<int:theReferenceId>/<int:QuestionGroupId>/', TheReferenceView.assignQuestionGroup, name='assignQuestionGroup'),
	path('unassignQuestionGroup/<int:theReferenceId>/', TheReferenceView.unassignQuestionGroup, name='unassignQuestionGroup'),
	path('assignUser/<int:theReferenceId>/<int:UserId>/', TheReferenceView.assignUser, name='assignUser'),
	path('unassignUser/<int:theReferenceId>/', TheReferenceView.unassignUser, name='unassignUser'),
	path('assignReferrer/<int:theReferenceId>/<int:ReferrerId>/', TheReferenceView.assignReferrer, name='assignReferrer'),
	path('unassignReferrer/<int:theReferenceId>/', TheReferenceView.unassignReferrer, name='unassignReferrer'),
	path('assignLastQuestionAnswered/<int:theReferenceId>/<int:LastQuestionAnsweredId>/', TheReferenceView.assignLastQuestionAnswered, name='assignLastQuestionAnswered'),
	path('unassignLastQuestionAnswered/<int:theReferenceId>/', TheReferenceView.unassignLastQuestionAnswered, name='unassignLastQuestionAnswered'),
	path('addAnswers/<int:theReferenceId>/<AnswersIds>/', TheReferenceView.addAnswers, name='addAnswers'),
	path('removeAnswers/<int:theReferenceId>/<AnswersIds>/', TheReferenceView.removeAnswers, name='removeAnswers'),
]
