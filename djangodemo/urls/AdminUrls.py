from django.urls import path
from djangodemo.views import AdminView

urlpatterns = [
    path('', AdminView.index, name='index'),
	path('create', AdminView.get, name='create'),
	path('get/<int:adminId>/', AdminView.get, name='get'),
	path('save', AdminView.save, name='save'),
	path('getAll', AdminView.getAll, name='getAll'),
	path('delete/<int:adminId>/', AdminView.delete, name='delete'),
	path('addUsers/<int:adminId>/<UsersIds>/', AdminView.addUsers, name='addUsers'),
	path('removeUsers/<int:adminId>/<UsersIds>/', AdminView.removeUsers, name='removeUsers'),
	path('addReferenceEngines/<int:adminId>/<ReferenceEnginesIds>/', AdminView.addReferenceEngines, name='addReferenceEngines'),
	path('removeReferenceEngines/<int:adminId>/<ReferenceEnginesIds>/', AdminView.removeReferenceEngines, name='removeReferenceEngines'),
]
