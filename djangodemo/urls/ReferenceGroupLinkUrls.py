from django.urls import path
from djangodemo.views import ReferenceGroupLinkView

urlpatterns = [
    path('', ReferenceGroupLinkView.index, name='index'),
	path('create', ReferenceGroupLinkView.get, name='create'),
	path('get/<int:referenceGroupLinkId>/', ReferenceGroupLinkView.get, name='get'),
	path('save', ReferenceGroupLinkView.save, name='save'),
	path('getAll', ReferenceGroupLinkView.getAll, name='getAll'),
	path('delete/<int:referenceGroupLinkId>/', ReferenceGroupLinkView.delete, name='delete'),
	path('assignReferrerGroup/<int:referenceGroupLinkId>/<int:ReferrerGroupId>/', ReferenceGroupLinkView.assignReferrerGroup, name='assignReferrerGroup'),
	path('unassignReferrerGroup/<int:referenceGroupLinkId>/', ReferenceGroupLinkView.unassignReferrerGroup, name='unassignReferrerGroup'),
	path('assignLinkProvider/<int:referenceGroupLinkId>/<int:LinkProviderId>/', ReferenceGroupLinkView.assignLinkProvider, name='assignLinkProvider'),
	path('unassignLinkProvider/<int:referenceGroupLinkId>/', ReferenceGroupLinkView.unassignLinkProvider, name='unassignLinkProvider'),
]
