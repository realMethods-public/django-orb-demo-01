from django.urls import path
from djangodemo.views import CommentView

urlpatterns = [
    path('', CommentView.index, name='index'),
	path('create', CommentView.get, name='create'),
	path('get/<int:commentId>/', CommentView.get, name='get'),
	path('save', CommentView.save, name='save'),
	path('getAll', CommentView.getAll, name='getAll'),
	path('delete/<int:commentId>/', CommentView.delete, name='delete'),
	path('assignSource/<int:commentId>/<int:SourceId>/', CommentView.assignSource, name='assignSource'),
	path('unassignSource/<int:commentId>/', CommentView.unassignSource, name='unassignSource'),
]
