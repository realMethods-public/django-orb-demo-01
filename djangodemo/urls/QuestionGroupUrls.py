from django.urls import path
from djangodemo.views import QuestionGroupView

urlpatterns = [
    path('', QuestionGroupView.index, name='index'),
	path('create', QuestionGroupView.get, name='create'),
	path('get/<int:questionGroupId>/', QuestionGroupView.get, name='get'),
	path('save', QuestionGroupView.save, name='save'),
	path('getAll', QuestionGroupView.getAll, name='getAll'),
	path('delete/<int:questionGroupId>/', QuestionGroupView.delete, name='delete'),
	path('addQuestions/<int:questionGroupId>/<QuestionsIds>/', QuestionGroupView.addQuestions, name='addQuestions'),
	path('removeQuestions/<int:questionGroupId>/<QuestionsIds>/', QuestionGroupView.removeQuestions, name='removeQuestions'),
	path('addQuestionGroups/<int:questionGroupId>/<QuestionGroupsIds>/', QuestionGroupView.addQuestionGroups, name='addQuestionGroups'),
	path('removeQuestionGroups/<int:questionGroupId>/<QuestionGroupsIds>/', QuestionGroupView.removeQuestionGroups, name='removeQuestionGroups'),
]
