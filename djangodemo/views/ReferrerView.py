import json

from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse

from djangodemo.delegates.ReferrerDelegate import ReferrerDelegate

 #======================================================================
# 
# Encapsulates data for View Referrer
#
# @author 
#
#======================================================================

#======================================================================
# Class ReferrerView function declarations
#======================================================================
def index(request):
	return HttpResponse("Hello, world. You're at the Referrer index.")

def get(request, referrerId ):
	delegate = ReferrerDelegate()
	responseData = delegate.get( referrerId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def create(request):
	referrer = json.loads(request.body)
	delegate = ReferrerDelegate()
	responseData = delegate.createFromJson( referrer )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def save(request):
	referrer = json.loads(request.body)
	delegate = ReferrerDelegate()
	responseData = delegate.save( referrer )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def delete(request, referrerId ):
	delegate = ReferrerDelegate()
	responseData = delegate.delete( referrerId )
	return HttpResponse(responseData, content_type="application/json");

def getAll(request):
	delegate = ReferrerDelegate()
	responseData = delegate.getAll()
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def addComments( request, referrerId, CommentsIds ):
	delegate = ReferrerDelegate()
	responseData = delegate.addComments( referrerId, CommentsIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def removeComments( request, referrerId, CommentsIds ):
	delegate = ReferrerDelegate()
	responseData = delegate.removeComments( referrerId, CommentsIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

