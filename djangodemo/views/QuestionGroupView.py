import json

from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse

from djangodemo.delegates.QuestionGroupDelegate import QuestionGroupDelegate

 #======================================================================
# 
# Encapsulates data for View QuestionGroup
#
# @author 
#
#======================================================================

#======================================================================
# Class QuestionGroupView function declarations
#======================================================================
def index(request):
	return HttpResponse("Hello, world. You're at the QuestionGroup index.")

def get(request, questionGroupId ):
	delegate = QuestionGroupDelegate()
	responseData = delegate.get( questionGroupId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def create(request):
	questionGroup = json.loads(request.body)
	delegate = QuestionGroupDelegate()
	responseData = delegate.createFromJson( questionGroup )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def save(request):
	questionGroup = json.loads(request.body)
	delegate = QuestionGroupDelegate()
	responseData = delegate.save( questionGroup )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def delete(request, questionGroupId ):
	delegate = QuestionGroupDelegate()
	responseData = delegate.delete( questionGroupId )
	return HttpResponse(responseData, content_type="application/json");

def getAll(request):
	delegate = QuestionGroupDelegate()
	responseData = delegate.getAll()
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def addQuestions( request, questionGroupId, QuestionsIds ):
	delegate = QuestionGroupDelegate()
	responseData = delegate.addQuestions( questionGroupId, QuestionsIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def removeQuestions( request, questionGroupId, QuestionsIds ):
	delegate = QuestionGroupDelegate()
	responseData = delegate.removeQuestions( questionGroupId, QuestionsIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def addQuestionGroups( request, questionGroupId, QuestionGroupsIds ):
	delegate = QuestionGroupDelegate()
	responseData = delegate.addQuestionGroups( questionGroupId, QuestionGroupsIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def removeQuestionGroups( request, questionGroupId, QuestionGroupsIds ):
	delegate = QuestionGroupDelegate()
	responseData = delegate.removeQuestionGroups( questionGroupId, QuestionGroupsIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

