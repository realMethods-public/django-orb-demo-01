import json

from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse

from djangodemo.delegates.AdminDelegate import AdminDelegate

 #======================================================================
# 
# Encapsulates data for View Admin
#
# @author 
#
#======================================================================

#======================================================================
# Class AdminView function declarations
#======================================================================
def index(request):
	return HttpResponse("Hello, world. You're at the Admin index.")

def get(request, adminId ):
	delegate = AdminDelegate()
	responseData = delegate.get( adminId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def create(request):
	admin = json.loads(request.body)
	delegate = AdminDelegate()
	responseData = delegate.createFromJson( admin )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def save(request):
	admin = json.loads(request.body)
	delegate = AdminDelegate()
	responseData = delegate.save( admin )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def delete(request, adminId ):
	delegate = AdminDelegate()
	responseData = delegate.delete( adminId )
	return HttpResponse(responseData, content_type="application/json");

def getAll(request):
	delegate = AdminDelegate()
	responseData = delegate.getAll()
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def addUsers( request, adminId, UsersIds ):
	delegate = AdminDelegate()
	responseData = delegate.addUsers( adminId, UsersIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def removeUsers( request, adminId, UsersIds ):
	delegate = AdminDelegate()
	responseData = delegate.removeUsers( adminId, UsersIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def addReferenceEngines( request, adminId, ReferenceEnginesIds ):
	delegate = AdminDelegate()
	responseData = delegate.addReferenceEngines( adminId, ReferenceEnginesIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def removeReferenceEngines( request, adminId, ReferenceEnginesIds ):
	delegate = AdminDelegate()
	responseData = delegate.removeReferenceEngines( adminId, ReferenceEnginesIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

