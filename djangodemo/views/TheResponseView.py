import json

from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse

from djangodemo.delegates.TheResponseDelegate import TheResponseDelegate

 #======================================================================
# 
# Encapsulates data for View TheResponse
#
# @author 
#
#======================================================================

#======================================================================
# Class TheResponseView function declarations
#======================================================================
def index(request):
	return HttpResponse("Hello, world. You're at the TheResponse index.")

def get(request, theResponseId ):
	delegate = TheResponseDelegate()
	responseData = delegate.get( theResponseId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def create(request):
	theResponse = json.loads(request.body)
	delegate = TheResponseDelegate()
	responseData = delegate.createFromJson( theResponse )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def save(request):
	theResponse = json.loads(request.body)
	delegate = TheResponseDelegate()
	responseData = delegate.save( theResponse )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def delete(request, theResponseId ):
	delegate = TheResponseDelegate()
	responseData = delegate.delete( theResponseId )
	return HttpResponse(responseData, content_type="application/json");

def getAll(request):
	delegate = TheResponseDelegate()
	responseData = delegate.getAll()
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

