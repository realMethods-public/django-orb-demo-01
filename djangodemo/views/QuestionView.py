import json

from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse

from djangodemo.delegates.QuestionDelegate import QuestionDelegate

 #======================================================================
# 
# Encapsulates data for View Question
#
# @author 
#
#======================================================================

#======================================================================
# Class QuestionView function declarations
#======================================================================
def index(request):
	return HttpResponse("Hello, world. You're at the Question index.")

def get(request, questionId ):
	delegate = QuestionDelegate()
	responseData = delegate.get( questionId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def create(request):
	question = json.loads(request.body)
	delegate = QuestionDelegate()
	responseData = delegate.createFromJson( question )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def save(request):
	question = json.loads(request.body)
	delegate = QuestionDelegate()
	responseData = delegate.save( question )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def delete(request, questionId ):
	delegate = QuestionDelegate()
	responseData = delegate.delete( questionId )
	return HttpResponse(responseData, content_type="application/json");

def getAll(request):
	delegate = QuestionDelegate()
	responseData = delegate.getAll()
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def addResponses( request, questionId, ResponsesIds ):
	delegate = QuestionDelegate()
	responseData = delegate.addResponses( questionId, ResponsesIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def removeResponses( request, questionId, ResponsesIds ):
	delegate = QuestionDelegate()
	responseData = delegate.removeResponses( questionId, ResponsesIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

