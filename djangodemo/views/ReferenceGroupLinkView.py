import json

from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse

from djangodemo.delegates.ReferenceGroupLinkDelegate import ReferenceGroupLinkDelegate

 #======================================================================
# 
# Encapsulates data for View ReferenceGroupLink
#
# @author 
#
#======================================================================

#======================================================================
# Class ReferenceGroupLinkView function declarations
#======================================================================
def index(request):
	return HttpResponse("Hello, world. You're at the ReferenceGroupLink index.")

def get(request, referenceGroupLinkId ):
	delegate = ReferenceGroupLinkDelegate()
	responseData = delegate.get( referenceGroupLinkId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def create(request):
	referenceGroupLink = json.loads(request.body)
	delegate = ReferenceGroupLinkDelegate()
	responseData = delegate.createFromJson( referenceGroupLink )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def save(request):
	referenceGroupLink = json.loads(request.body)
	delegate = ReferenceGroupLinkDelegate()
	responseData = delegate.save( referenceGroupLink )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def delete(request, referenceGroupLinkId ):
	delegate = ReferenceGroupLinkDelegate()
	responseData = delegate.delete( referenceGroupLinkId )
	return HttpResponse(responseData, content_type="application/json");

def getAll(request):
	delegate = ReferenceGroupLinkDelegate()
	responseData = delegate.getAll()
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def assignReferrerGroup( request, referenceGroupLinkId, ReferrerGroupId ):
	delegate = ReferenceGroupLinkDelegate()
	responseData = delegate.saveReferrerGroup( referenceGroupLinkId, ReferrerGroupId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");
	
def unassignReferrerGroup( request, referenceGroupLinkId ):
	delegate = ReferenceGroupLinkDelegate()
	responseData = delegate.deleteReferrerGroup( referenceGroupLinkId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def assignLinkProvider( request, referenceGroupLinkId, LinkProviderId ):
	delegate = ReferenceGroupLinkDelegate()
	responseData = delegate.saveLinkProvider( referenceGroupLinkId, LinkProviderId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");
	
def unassignLinkProvider( request, referenceGroupLinkId ):
	delegate = ReferenceGroupLinkDelegate()
	responseData = delegate.deleteLinkProvider( referenceGroupLinkId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

