import json

from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse

from djangodemo.delegates.AnswerDelegate import AnswerDelegate

 #======================================================================
# 
# Encapsulates data for View Answer
#
# @author 
#
#======================================================================

#======================================================================
# Class AnswerView function declarations
#======================================================================
def index(request):
	return HttpResponse("Hello, world. You're at the Answer index.")

def get(request, answerId ):
	delegate = AnswerDelegate()
	responseData = delegate.get( answerId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def create(request):
	answer = json.loads(request.body)
	delegate = AnswerDelegate()
	responseData = delegate.createFromJson( answer )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def save(request):
	answer = json.loads(request.body)
	delegate = AnswerDelegate()
	responseData = delegate.save( answer )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def delete(request, answerId ):
	delegate = AnswerDelegate()
	responseData = delegate.delete( answerId )
	return HttpResponse(responseData, content_type="application/json");

def getAll(request):
	delegate = AnswerDelegate()
	responseData = delegate.getAll()
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def assignQuestion( request, answerId, QuestionId ):
	delegate = AnswerDelegate()
	responseData = delegate.saveQuestion( answerId, QuestionId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");
	
def unassignQuestion( request, answerId ):
	delegate = AnswerDelegate()
	responseData = delegate.deleteQuestion( answerId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def assignResponse( request, answerId, ResponseId ):
	delegate = AnswerDelegate()
	responseData = delegate.saveResponse( answerId, ResponseId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");
	
def unassignResponse( request, answerId ):
	delegate = AnswerDelegate()
	responseData = delegate.deleteResponse( answerId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

