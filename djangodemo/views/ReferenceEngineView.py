import json

from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse

from djangodemo.delegates.ReferenceEngineDelegate import ReferenceEngineDelegate

 #======================================================================
# 
# Encapsulates data for View ReferenceEngine
#
# @author 
#
#======================================================================

#======================================================================
# Class ReferenceEngineView function declarations
#======================================================================
def index(request):
	return HttpResponse("Hello, world. You're at the ReferenceEngine index.")

def get(request, referenceEngineId ):
	delegate = ReferenceEngineDelegate()
	responseData = delegate.get( referenceEngineId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def create(request):
	referenceEngine = json.loads(request.body)
	delegate = ReferenceEngineDelegate()
	responseData = delegate.createFromJson( referenceEngine )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def save(request):
	referenceEngine = json.loads(request.body)
	delegate = ReferenceEngineDelegate()
	responseData = delegate.save( referenceEngine )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def delete(request, referenceEngineId ):
	delegate = ReferenceEngineDelegate()
	responseData = delegate.delete( referenceEngineId )
	return HttpResponse(responseData, content_type="application/json");

def getAll(request):
	delegate = ReferenceEngineDelegate()
	responseData = delegate.getAll()
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def assignMainQuestionGroup( request, referenceEngineId, MainQuestionGroupId ):
	delegate = ReferenceEngineDelegate()
	responseData = delegate.saveMainQuestionGroup( referenceEngineId, MainQuestionGroupId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");
	
def unassignMainQuestionGroup( request, referenceEngineId ):
	delegate = ReferenceEngineDelegate()
	responseData = delegate.deleteMainQuestionGroup( referenceEngineId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

