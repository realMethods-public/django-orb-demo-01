import json

from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse

from djangodemo.delegates.UserDelegate import UserDelegate

 #======================================================================
# 
# Encapsulates data for View User
#
# @author 
#
#======================================================================

#======================================================================
# Class UserView function declarations
#======================================================================
def index(request):
	return HttpResponse("Hello, world. You're at the User index.")

def get(request, userId ):
	delegate = UserDelegate()
	responseData = delegate.get( userId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def create(request):
	user = json.loads(request.body)
	delegate = UserDelegate()
	responseData = delegate.createFromJson( user )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def save(request):
	user = json.loads(request.body)
	delegate = UserDelegate()
	responseData = delegate.save( user )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def delete(request, userId ):
	delegate = UserDelegate()
	responseData = delegate.delete( userId )
	return HttpResponse(responseData, content_type="application/json");

def getAll(request):
	delegate = UserDelegate()
	responseData = delegate.getAll()
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def addReferenceProviders( request, userId, ReferenceProvidersIds ):
	delegate = UserDelegate()
	responseData = delegate.addReferenceProviders( userId, ReferenceProvidersIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def removeReferenceProviders( request, userId, ReferenceProvidersIds ):
	delegate = UserDelegate()
	responseData = delegate.removeReferenceProviders( userId, ReferenceProvidersIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def addRefererGroups( request, userId, RefererGroupsIds ):
	delegate = UserDelegate()
	responseData = delegate.addRefererGroups( userId, RefererGroupsIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def removeRefererGroups( request, userId, RefererGroupsIds ):
	delegate = UserDelegate()
	responseData = delegate.removeRefererGroups( userId, RefererGroupsIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def addReferenceReceivers( request, userId, ReferenceReceiversIds ):
	delegate = UserDelegate()
	responseData = delegate.addReferenceReceivers( userId, ReferenceReceiversIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def removeReferenceReceivers( request, userId, ReferenceReceiversIds ):
	delegate = UserDelegate()
	responseData = delegate.removeReferenceReceivers( userId, ReferenceReceiversIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def addReferenceGroupLinks( request, userId, ReferenceGroupLinksIds ):
	delegate = UserDelegate()
	responseData = delegate.addReferenceGroupLinks( userId, ReferenceGroupLinksIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def removeReferenceGroupLinks( request, userId, ReferenceGroupLinksIds ):
	delegate = UserDelegate()
	responseData = delegate.removeReferenceGroupLinks( userId, ReferenceGroupLinksIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

