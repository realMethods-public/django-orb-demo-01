import json

from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse

from djangodemo.delegates.CommentDelegate import CommentDelegate

 #======================================================================
# 
# Encapsulates data for View Comment
#
# @author 
#
#======================================================================

#======================================================================
# Class CommentView function declarations
#======================================================================
def index(request):
	return HttpResponse("Hello, world. You're at the Comment index.")

def get(request, commentId ):
	delegate = CommentDelegate()
	responseData = delegate.get( commentId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def create(request):
	comment = json.loads(request.body)
	delegate = CommentDelegate()
	responseData = delegate.createFromJson( comment )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def save(request):
	comment = json.loads(request.body)
	delegate = CommentDelegate()
	responseData = delegate.save( comment )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def delete(request, commentId ):
	delegate = CommentDelegate()
	responseData = delegate.delete( commentId )
	return HttpResponse(responseData, content_type="application/json");

def getAll(request):
	delegate = CommentDelegate()
	responseData = delegate.getAll()
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def assignSource( request, commentId, SourceId ):
	delegate = CommentDelegate()
	responseData = delegate.saveSource( commentId, SourceId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");
	
def unassignSource( request, commentId ):
	delegate = CommentDelegate()
	responseData = delegate.deleteSource( commentId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

