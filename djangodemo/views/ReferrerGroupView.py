import json

from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse

from djangodemo.delegates.ReferrerGroupDelegate import ReferrerGroupDelegate

 #======================================================================
# 
# Encapsulates data for View ReferrerGroup
#
# @author 
#
#======================================================================

#======================================================================
# Class ReferrerGroupView function declarations
#======================================================================
def index(request):
	return HttpResponse("Hello, world. You're at the ReferrerGroup index.")

def get(request, referrerGroupId ):
	delegate = ReferrerGroupDelegate()
	responseData = delegate.get( referrerGroupId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def create(request):
	referrerGroup = json.loads(request.body)
	delegate = ReferrerGroupDelegate()
	responseData = delegate.createFromJson( referrerGroup )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def save(request):
	referrerGroup = json.loads(request.body)
	delegate = ReferrerGroupDelegate()
	responseData = delegate.save( referrerGroup )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def delete(request, referrerGroupId ):
	delegate = ReferrerGroupDelegate()
	responseData = delegate.delete( referrerGroupId )
	return HttpResponse(responseData, content_type="application/json");

def getAll(request):
	delegate = ReferrerGroupDelegate()
	responseData = delegate.getAll()
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def addReferences( request, referrerGroupId, ReferencesIds ):
	delegate = ReferrerGroupDelegate()
	responseData = delegate.addReferences( referrerGroupId, ReferencesIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def removeReferences( request, referrerGroupId, ReferencesIds ):
	delegate = ReferrerGroupDelegate()
	responseData = delegate.removeReferences( referrerGroupId, ReferencesIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

