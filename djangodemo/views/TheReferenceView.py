import json

from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse

from djangodemo.delegates.TheReferenceDelegate import TheReferenceDelegate

 #======================================================================
# 
# Encapsulates data for View TheReference
#
# @author 
#
#======================================================================

#======================================================================
# Class TheReferenceView function declarations
#======================================================================
def index(request):
	return HttpResponse("Hello, world. You're at the TheReference index.")

def get(request, theReferenceId ):
	delegate = TheReferenceDelegate()
	responseData = delegate.get( theReferenceId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def create(request):
	theReference = json.loads(request.body)
	delegate = TheReferenceDelegate()
	responseData = delegate.createFromJson( theReference )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def save(request):
	theReference = json.loads(request.body)
	delegate = TheReferenceDelegate()
	responseData = delegate.save( theReference )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def delete(request, theReferenceId ):
	delegate = TheReferenceDelegate()
	responseData = delegate.delete( theReferenceId )
	return HttpResponse(responseData, content_type="application/json");

def getAll(request):
	delegate = TheReferenceDelegate()
	responseData = delegate.getAll()
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def assignQuestionGroup( request, theReferenceId, QuestionGroupId ):
	delegate = TheReferenceDelegate()
	responseData = delegate.saveQuestionGroup( theReferenceId, QuestionGroupId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");
	
def unassignQuestionGroup( request, theReferenceId ):
	delegate = TheReferenceDelegate()
	responseData = delegate.deleteQuestionGroup( theReferenceId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def assignUser( request, theReferenceId, UserId ):
	delegate = TheReferenceDelegate()
	responseData = delegate.saveUser( theReferenceId, UserId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");
	
def unassignUser( request, theReferenceId ):
	delegate = TheReferenceDelegate()
	responseData = delegate.deleteUser( theReferenceId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def assignReferrer( request, theReferenceId, ReferrerId ):
	delegate = TheReferenceDelegate()
	responseData = delegate.saveReferrer( theReferenceId, ReferrerId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");
	
def unassignReferrer( request, theReferenceId ):
	delegate = TheReferenceDelegate()
	responseData = delegate.deleteReferrer( theReferenceId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def assignLastQuestionAnswered( request, theReferenceId, LastQuestionAnsweredId ):
	delegate = TheReferenceDelegate()
	responseData = delegate.saveLastQuestionAnswered( theReferenceId, LastQuestionAnsweredId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");
	
def unassignLastQuestionAnswered( request, theReferenceId ):
	delegate = TheReferenceDelegate()
	responseData = delegate.deleteLastQuestionAnswered( theReferenceId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def addAnswers( request, theReferenceId, AnswersIds ):
	delegate = TheReferenceDelegate()
	responseData = delegate.addAnswers( theReferenceId, AnswersIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def removeAnswers( request, theReferenceId, AnswersIds ):
	delegate = TheReferenceDelegate()
	responseData = delegate.removeAnswers( theReferenceId, AnswersIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

