import json

from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse

from djangodemo.delegates.ActivityDelegate import ActivityDelegate

 #======================================================================
# 
# Encapsulates data for View Activity
#
# @author 
#
#======================================================================

#======================================================================
# Class ActivityView function declarations
#======================================================================
def index(request):
	return HttpResponse("Hello, world. You're at the Activity index.")

def get(request, activityId ):
	delegate = ActivityDelegate()
	responseData = delegate.get( activityId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def create(request):
	activity = json.loads(request.body)
	delegate = ActivityDelegate()
	responseData = delegate.createFromJson( activity )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def save(request):
	activity = json.loads(request.body)
	delegate = ActivityDelegate()
	responseData = delegate.save( activity )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def delete(request, activityId ):
	delegate = ActivityDelegate()
	responseData = delegate.delete( activityId )
	return HttpResponse(responseData, content_type="application/json");

def getAll(request):
	delegate = ActivityDelegate()
	responseData = delegate.getAll()
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def assignUser( request, activityId, UserId ):
	delegate = ActivityDelegate()
	responseData = delegate.saveUser( activityId, UserId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");
	
def unassignUser( request, activityId ):
	delegate = ActivityDelegate()
	responseData = delegate.deleteUser( activityId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

