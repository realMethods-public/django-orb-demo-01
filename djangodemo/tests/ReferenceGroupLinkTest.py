import datetime

from django.test import TestCase
from django.utils import timezone
from djangodemo.models.ReferenceGroupLink import ReferenceGroupLink
from djangodemo.delegates.ReferenceGroupLinkDelegate import ReferenceGroupLinkDelegate

 #======================================================================
# 
# Encapsulates data for model ReferenceGroupLink
#
# @author 
#
#======================================================================

#======================================================================
# Class ReferenceGroupLinkTest Declaration
#======================================================================
class ReferenceGroupLinkTest (TestCase) :
	def test_crud(self) :
		referenceGroupLink = ReferenceGroupLink()
		referenceGroupLink.dateLinkCreated = "default dateLinkCreated field value"
		referenceGroupLink.name = "default name field value"
		
		delegate = ReferenceGroupLinkDelegate()
		responseObj = delegate.create(referenceGroupLink)
		
		self.assertEqual(responseObj, delegate.get( responseObj.id ))
	
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 1 )		
		delegate.delete(responseObj.id)
		
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 0 )		


