import datetime

from django.test import TestCase
from django.utils import timezone
from djangodemo.models.ReferenceEngine import ReferenceEngine
from djangodemo.delegates.ReferenceEngineDelegate import ReferenceEngineDelegate

 #======================================================================
# 
# Encapsulates data for model ReferenceEngine
#
# @author 
#
#======================================================================

#======================================================================
# Class ReferenceEngineTest Declaration
#======================================================================
class ReferenceEngineTest (TestCase) :
	def test_crud(self) :
		referenceEngine = ReferenceEngine()
		referenceEngine.name = "default name field value"
		referenceEngine.active = False
		referenceEngine.purpose = "default purpose field value"
		
		delegate = ReferenceEngineDelegate()
		responseObj = delegate.create(referenceEngine)
		
		self.assertEqual(responseObj, delegate.get( responseObj.id ))
	
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 1 )		
		delegate.delete(responseObj.id)
		
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 0 )		


