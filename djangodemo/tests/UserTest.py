import datetime

from django.test import TestCase
from django.utils import timezone
from djangodemo.models.User import User
from djangodemo.delegates.UserDelegate import UserDelegate

 #======================================================================
# 
# Encapsulates data for model User
#
# @author 
#
#======================================================================

#======================================================================
# Class UserTest Declaration
#======================================================================
class UserTest (TestCase) :
	def test_crud(self) :
		user = User()
		user.password = "default password field value"
		user.resumeLinkUrl = "default resumeLinkUrl field value"
		user.linkedInUrl = "default linkedInUrl field value"
		
		delegate = UserDelegate()
		responseObj = delegate.create(user)
		
		self.assertEqual(responseObj, delegate.get( responseObj.id ))
	
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 1 )		
		delegate.delete(responseObj.id)
		
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 0 )		


