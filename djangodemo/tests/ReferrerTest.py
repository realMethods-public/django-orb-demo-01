import datetime

from django.test import TestCase
from django.utils import timezone
from djangodemo.models.Referrer import Referrer
from djangodemo.delegates.ReferrerDelegate import ReferrerDelegate

 #======================================================================
# 
# Encapsulates data for model Referrer
#
# @author 
#
#======================================================================

#======================================================================
# Class ReferrerTest Declaration
#======================================================================
class ReferrerTest (TestCase) :
	def test_crud(self) :
		referrer = Referrer()
		referrer.firstName = "default firstName field value"
		referrer.lastName = "default lastName field value"
		referrer.emailAddress = "default emailAddress field value"
		referrer.active = False
		
		delegate = ReferrerDelegate()
		responseObj = delegate.create(referrer)
		
		self.assertEqual(responseObj, delegate.get( responseObj.id ))
	
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 1 )		
		delegate.delete(responseObj.id)
		
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 0 )		


