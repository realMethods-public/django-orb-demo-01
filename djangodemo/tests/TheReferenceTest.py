import datetime

from django.test import TestCase
from django.utils import timezone
from djangodemo.models.TheReference import TheReference
from djangodemo.delegates.TheReferenceDelegate import TheReferenceDelegate

 #======================================================================
# 
# Encapsulates data for model TheReference
#
# @author 
#
#======================================================================

#======================================================================
# Class TheReferenceTest Declaration
#======================================================================
class TheReferenceTest (TestCase) :
	def test_crud(self) :
		theReference = TheReference()
		theReference.dateLastUpdated = "default dateLastUpdated field value"
		theReference.active = False
		theReference.dateTimeLastViewedExternally = "default dateTimeLastViewedExternally field value"
		theReference.makeViewableToUser = False
		theReference.dateLastSent = "default dateLastSent field value"
		theReference.numDaysToExpire = 22
		theReference.status = "default status field value"
		theReference.type = "default type field value"
		
		delegate = TheReferenceDelegate()
		responseObj = delegate.create(theReference)
		
		self.assertEqual(responseObj, delegate.get( responseObj.id ))
	
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 1 )		
		delegate.delete(responseObj.id)
		
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 0 )		


