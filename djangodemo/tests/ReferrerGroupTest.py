import datetime

from django.test import TestCase
from django.utils import timezone
from djangodemo.models.ReferrerGroup import ReferrerGroup
from djangodemo.delegates.ReferrerGroupDelegate import ReferrerGroupDelegate

 #======================================================================
# 
# Encapsulates data for model ReferrerGroup
#
# @author 
#
#======================================================================

#======================================================================
# Class ReferrerGroupTest Declaration
#======================================================================
class ReferrerGroupTest (TestCase) :
	def test_crud(self) :
		referrerGroup = ReferrerGroup()
		referrerGroup.name = "default name field value"
		referrerGroup.dateTimeLastViewedExternally = "default dateTimeLastViewedExternally field value"
		
		delegate = ReferrerGroupDelegate()
		responseObj = delegate.create(referrerGroup)
		
		self.assertEqual(responseObj, delegate.get( responseObj.id ))
	
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 1 )		
		delegate.delete(responseObj.id)
		
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 0 )		


