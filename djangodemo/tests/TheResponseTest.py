import datetime

from django.test import TestCase
from django.utils import timezone
from djangodemo.models.TheResponse import TheResponse
from djangodemo.delegates.TheResponseDelegate import TheResponseDelegate

 #======================================================================
# 
# Encapsulates data for model TheResponse
#
# @author 
#
#======================================================================

#======================================================================
# Class TheResponseTest Declaration
#======================================================================
class TheResponseTest (TestCase) :
	def test_crud(self) :
		theResponse = TheResponse()
		theResponse.responseText = "default responseText field value"
		theResponse.gotoQuestionId = "default gotoQuestionId field value"
		
		delegate = TheResponseDelegate()
		responseObj = delegate.create(theResponse)
		
		self.assertEqual(responseObj, delegate.get( responseObj.id ))
	
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 1 )		
		delegate.delete(responseObj.id)
		
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 0 )		


