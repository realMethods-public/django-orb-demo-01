import datetime

from django.test import TestCase
from django.utils import timezone
from djangodemo.models.QuestionGroup import QuestionGroup
from djangodemo.delegates.QuestionGroupDelegate import QuestionGroupDelegate

 #======================================================================
# 
# Encapsulates data for model QuestionGroup
#
# @author 
#
#======================================================================

#======================================================================
# Class QuestionGroupTest Declaration
#======================================================================
class QuestionGroupTest (TestCase) :
	def test_crud(self) :
		questionGroup = QuestionGroup()
		questionGroup.aggregateScore = "default aggregateScore field value"
		questionGroup.weight = "default weight field value"
		questionGroup.name = "default name field value"
		
		delegate = QuestionGroupDelegate()
		responseObj = delegate.create(questionGroup)
		
		self.assertEqual(responseObj, delegate.get( responseObj.id ))
	
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 1 )		
		delegate.delete(responseObj.id)
		
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 0 )		


