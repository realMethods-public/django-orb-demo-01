import datetime

from django.test import TestCase
from django.utils import timezone
from djangodemo.models.Question import Question
from djangodemo.delegates.QuestionDelegate import QuestionDelegate

 #======================================================================
# 
# Encapsulates data for model Question
#
# @author 
#
#======================================================================

#======================================================================
# Class QuestionTest Declaration
#======================================================================
class QuestionTest (TestCase) :
	def test_crud(self) :
		question = Question()
		question.weight = "default weight field value"
		question.questionText = "default questionText field value"
		question.identifier = "default identifier field value"
		question.mustBeAnswered = False
		question.responseExclusive = False
		
		delegate = QuestionDelegate()
		responseObj = delegate.create(question)
		
		self.assertEqual(responseObj, delegate.get( responseObj.id ))
	
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 1 )		
		delegate.delete(responseObj.id)
		
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 0 )		


