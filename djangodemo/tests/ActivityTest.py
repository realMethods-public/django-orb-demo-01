import datetime

from django.test import TestCase
from django.utils import timezone
from djangodemo.models.Activity import Activity
from djangodemo.delegates.ActivityDelegate import ActivityDelegate

 #======================================================================
# 
# Encapsulates data for model Activity
#
# @author 
#
#======================================================================

#======================================================================
# Class ActivityTest Declaration
#======================================================================
class ActivityTest (TestCase) :
	def test_crud(self) :
		activity = Activity()
		activity.refObjId = "default refObjId field value"
		activity.createDateTime = "default createDateTime field value"
		activity.type = "default type field value"
		
		delegate = ActivityDelegate()
		responseObj = delegate.create(activity)
		
		self.assertEqual(responseObj, delegate.get( responseObj.id ))
	
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 1 )		
		delegate.delete(responseObj.id)
		
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 0 )		


