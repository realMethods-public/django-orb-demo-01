import datetime

from django.test import TestCase
from django.utils import timezone
from djangodemo.models.Comment import Comment
from djangodemo.delegates.CommentDelegate import CommentDelegate

 #======================================================================
# 
# Encapsulates data for model Comment
#
# @author 
#
#======================================================================

#======================================================================
# Class CommentTest Declaration
#======================================================================
class CommentTest (TestCase) :
	def test_crud(self) :
		comment = Comment()
		comment.commentText = "default commentText field value"
		
		delegate = CommentDelegate()
		responseObj = delegate.create(comment)
		
		self.assertEqual(responseObj, delegate.get( responseObj.id ))
	
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 1 )		
		delegate.delete(responseObj.id)
		
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 0 )		


