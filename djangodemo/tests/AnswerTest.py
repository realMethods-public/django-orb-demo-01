import datetime

from django.test import TestCase
from django.utils import timezone
from djangodemo.models.Answer import Answer
from djangodemo.delegates.AnswerDelegate import AnswerDelegate

 #======================================================================
# 
# Encapsulates data for model Answer
#
# @author 
#
#======================================================================

#======================================================================
# Class AnswerTest Declaration
#======================================================================
class AnswerTest (TestCase) :
	def test_crud(self) :
		answer = Answer()
		
		delegate = AnswerDelegate()
		responseObj = delegate.create(answer)
		
		self.assertEqual(responseObj, delegate.get( responseObj.id ))
	
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 1 )		
		delegate.delete(responseObj.id)
		
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 0 )		


