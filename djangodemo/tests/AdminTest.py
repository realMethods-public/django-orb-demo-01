import datetime

from django.test import TestCase
from django.utils import timezone
from djangodemo.models.Admin import Admin
from djangodemo.delegates.AdminDelegate import AdminDelegate

 #======================================================================
# 
# Encapsulates data for model Admin
#
# @author 
#
#======================================================================

#======================================================================
# Class AdminTest Declaration
#======================================================================
class AdminTest (TestCase) :
	def test_crud(self) :
		admin = Admin()
		admin.loginId = "default loginId field value"
		admin.password = "default password field value"
		
		delegate = AdminDelegate()
		responseObj = delegate.create(admin)
		
		self.assertEqual(responseObj, delegate.get( responseObj.id ))
	
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 1 )		
		delegate.delete(responseObj.id)
		
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 0 )		


