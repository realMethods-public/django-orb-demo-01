from django.core import exceptions
from django.core import serializers
from django.db import models
from django.db import utils

from djangodemo.models.Answer import Answer
from djangodemo.models.Question import Question
from djangodemo.models.TheResponse import TheResponse
from djangodemo.exceptions import Exceptions

 #======================================================================
# 
# Encapsulates data for model Answer
#
# @author 
#
#======================================================================

#======================================================================
# Class AnswerDelegate Declaration
#======================================================================
class AnswerDelegate :

#======================================================================
# Function Declarations
#======================================================================

	def get(self, answerId ):
		try:	
			answer = Answer.objects.filter(id=answerId)
			return answer.first();
		except Answer.DoesNotExist:
			raise ProcessingError("Answer with id " + str(answerId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 

	def createFromJson(self, answer):
		for model in serializers.deserialize("json", answer):
			model.save()
			return model;

	def create(self, answer):
		answer.save()
		return answer;

	def saveFromJson(self, answer):
		for model in serializers.deserialize("json", answer):
			model.save()
			return answer;
	
	def save(self, answer):
		answer.save()
		return answer;
	
	def delete(self, answerId ):
		errMsg = "Failed to delete Answer from db using id " + str(answerId)
		
		try:
			answer = Answer.objects.get(id=answerId)
			answer.delete()
			return True
		except Answer.DoesNotExist:
			raise ProcessingError("Answer with id " + str(answerId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 
	
	def getAll(self):
		try:
			all = Answer.objects.all()
			return all;
		except utils.DatabaseError:
			raise StorageReadError("Failed to get all Answer from db")
		except Exception:
			return None;
		
	def assignQuestion( self, answerId, questionId ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.QuestionDelegate import QuestionDelegate

		errMsg = "Failed to assign element " + str(questionId) + " for Question on Answer"

		try:
			# get the Answer from db
			answer = self.get( answerId ).first()	
			
			# get the Question from db
			question = QuestionDelegate().get(questionId).first();
			
			# assign the Question		
			answer.question = question
			
			#save it
			answer.save()

			# reload and return the appropriate version					
			return self.get( answerId );
		except Answer.DoesNotExist:
			raise ProcessingError(errMsg + " : Answer with id " + str(answerId) + " does not exist.")
		except Question.DoesNotExist:
			raise ProcessingError(errMsg + " : Question with id " + str(questionId) + " does not exist.")
		except Exception:
			return None;
				
	def unassignQuestion( self, answerId ):
		errMsg = "Failed to unassign element " + str(questionId) + " for Question on Answer"

		try:
			# get the Answer from db
			answer = self.get( answerId ).first()	
			
			# assign to None for unassignment
			answer.question = None			

			#save it
			answer.save()

			# reload and return the appropriate version					
			return self.get( answerId );
		except Answer.DoesNotExist:
			raise ProcessingError(errMsg + " : Answer with id " + str(answerId) + " does not exist.")
		except Exception:
			return None;
		
	def assignResponse( self, answerId, responseId ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.TheResponseDelegate import TheResponseDelegate

		errMsg = "Failed to assign element " + str(responseId) + " for Response on Answer"

		try:
			# get the Answer from db
			answer = self.get( answerId ).first()	
			
			# get the TheResponse from db
			theResponse = TheResponseDelegate().get(responseId).first();
			
			# assign the Response		
			answer.response = theResponse
			
			#save it
			answer.save()

			# reload and return the appropriate version					
			return self.get( answerId );
		except Answer.DoesNotExist:
			raise ProcessingError(errMsg + " : Answer with id " + str(answerId) + " does not exist.")
		except TheResponse.DoesNotExist:
			raise ProcessingError(errMsg + " : TheResponse with id " + str(responseId) + " does not exist.")
		except Exception:
			return None;
				
	def unassignResponse( self, answerId ):
		errMsg = "Failed to unassign element " + str(responseId) + " for Response on Answer"

		try:
			# get the Answer from db
			answer = self.get( answerId ).first()	
			
			# assign to None for unassignment
			answer.theResponse = None			

			#save it
			answer.save()

			# reload and return the appropriate version					
			return self.get( answerId );
		except Answer.DoesNotExist:
			raise ProcessingError(errMsg + " : Answer with id " + str(answerId) + " does not exist.")
		except Exception:
			return None;
		
