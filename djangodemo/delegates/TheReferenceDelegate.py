from django.core import exceptions
from django.core import serializers
from django.db import models
from django.db import utils

from djangodemo.models.TheReference import TheReference
from djangodemo.models.QuestionGroup import QuestionGroup
from djangodemo.models.User import User
from djangodemo.models.Referrer import Referrer
from djangodemo.models.Answer import Answer
from djangodemo.models.Question import Question
from djangodemo.exceptions import Exceptions

 #======================================================================
# 
# Encapsulates data for model TheReference
#
# @author 
#
#======================================================================

#======================================================================
# Class TheReferenceDelegate Declaration
#======================================================================
class TheReferenceDelegate :

#======================================================================
# Function Declarations
#======================================================================

	def get(self, theReferenceId ):
		try:	
			theReference = TheReference.objects.filter(id=theReferenceId)
			return theReference.first();
		except TheReference.DoesNotExist:
			raise ProcessingError("TheReference with id " + str(theReferenceId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 

	def createFromJson(self, theReference):
		for model in serializers.deserialize("json", theReference):
			model.save()
			return model;

	def create(self, theReference):
		theReference.save()
		return theReference;

	def saveFromJson(self, theReference):
		for model in serializers.deserialize("json", theReference):
			model.save()
			return theReference;
	
	def save(self, theReference):
		theReference.save()
		return theReference;
	
	def delete(self, theReferenceId ):
		errMsg = "Failed to delete TheReference from db using id " + str(theReferenceId)
		
		try:
			theReference = TheReference.objects.get(id=theReferenceId)
			theReference.delete()
			return True
		except TheReference.DoesNotExist:
			raise ProcessingError("TheReference with id " + str(theReferenceId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 
	
	def getAll(self):
		try:
			all = TheReference.objects.all()
			return all;
		except utils.DatabaseError:
			raise StorageReadError("Failed to get all TheReference from db")
		except Exception:
			return None;
		
	def assignQuestionGroup( self, theReferenceId, questionGroupId ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.QuestionGroupDelegate import QuestionGroupDelegate

		errMsg = "Failed to assign element " + str(questionGroupId) + " for QuestionGroup on TheReference"

		try:
			# get the TheReference from db
			theReference = self.get( theReferenceId ).first()	
			
			# get the QuestionGroup from db
			questionGroup = QuestionGroupDelegate().get(questionGroupId).first();
			
			# assign the QuestionGroup		
			theReference.questionGroup = questionGroup
			
			#save it
			theReference.save()

			# reload and return the appropriate version					
			return self.get( theReferenceId );
		except TheReference.DoesNotExist:
			raise ProcessingError(errMsg + " : TheReference with id " + str(theReferenceId) + " does not exist.")
		except QuestionGroup.DoesNotExist:
			raise ProcessingError(errMsg + " : QuestionGroup with id " + str(questionGroupId) + " does not exist.")
		except Exception:
			return None;
				
	def unassignQuestionGroup( self, theReferenceId ):
		errMsg = "Failed to unassign element " + str(questionGroupId) + " for QuestionGroup on TheReference"

		try:
			# get the TheReference from db
			theReference = self.get( theReferenceId ).first()	
			
			# assign to None for unassignment
			theReference.questionGroup = None			

			#save it
			theReference.save()

			# reload and return the appropriate version					
			return self.get( theReferenceId );
		except TheReference.DoesNotExist:
			raise ProcessingError(errMsg + " : TheReference with id " + str(theReferenceId) + " does not exist.")
		except Exception:
			return None;
		
	def assignUser( self, theReferenceId, userId ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.UserDelegate import UserDelegate

		errMsg = "Failed to assign element " + str(userId) + " for User on TheReference"

		try:
			# get the TheReference from db
			theReference = self.get( theReferenceId ).first()	
			
			# get the User from db
			user = UserDelegate().get(userId).first();
			
			# assign the User		
			theReference.user = user
			
			#save it
			theReference.save()

			# reload and return the appropriate version					
			return self.get( theReferenceId );
		except TheReference.DoesNotExist:
			raise ProcessingError(errMsg + " : TheReference with id " + str(theReferenceId) + " does not exist.")
		except User.DoesNotExist:
			raise ProcessingError(errMsg + " : User with id " + str(userId) + " does not exist.")
		except Exception:
			return None;
				
	def unassignUser( self, theReferenceId ):
		errMsg = "Failed to unassign element " + str(userId) + " for User on TheReference"

		try:
			# get the TheReference from db
			theReference = self.get( theReferenceId ).first()	
			
			# assign to None for unassignment
			theReference.user = None			

			#save it
			theReference.save()

			# reload and return the appropriate version					
			return self.get( theReferenceId );
		except TheReference.DoesNotExist:
			raise ProcessingError(errMsg + " : TheReference with id " + str(theReferenceId) + " does not exist.")
		except Exception:
			return None;
		
	def assignReferrer( self, theReferenceId, referrerId ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.ReferrerDelegate import ReferrerDelegate

		errMsg = "Failed to assign element " + str(referrerId) + " for Referrer on TheReference"

		try:
			# get the TheReference from db
			theReference = self.get( theReferenceId ).first()	
			
			# get the Referrer from db
			referrer = ReferrerDelegate().get(referrerId).first();
			
			# assign the Referrer		
			theReference.referrer = referrer
			
			#save it
			theReference.save()

			# reload and return the appropriate version					
			return self.get( theReferenceId );
		except TheReference.DoesNotExist:
			raise ProcessingError(errMsg + " : TheReference with id " + str(theReferenceId) + " does not exist.")
		except Referrer.DoesNotExist:
			raise ProcessingError(errMsg + " : Referrer with id " + str(referrerId) + " does not exist.")
		except Exception:
			return None;
				
	def unassignReferrer( self, theReferenceId ):
		errMsg = "Failed to unassign element " + str(referrerId) + " for Referrer on TheReference"

		try:
			# get the TheReference from db
			theReference = self.get( theReferenceId ).first()	
			
			# assign to None for unassignment
			theReference.referrer = None			

			#save it
			theReference.save()

			# reload and return the appropriate version					
			return self.get( theReferenceId );
		except TheReference.DoesNotExist:
			raise ProcessingError(errMsg + " : TheReference with id " + str(theReferenceId) + " does not exist.")
		except Exception:
			return None;
		
	def assignLastQuestionAnswered( self, theReferenceId, lastQuestionAnsweredId ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.QuestionDelegate import QuestionDelegate

		errMsg = "Failed to assign element " + str(lastQuestionAnsweredId) + " for LastQuestionAnswered on TheReference"

		try:
			# get the TheReference from db
			theReference = self.get( theReferenceId ).first()	
			
			# get the Question from db
			question = QuestionDelegate().get(lastQuestionAnsweredId).first();
			
			# assign the LastQuestionAnswered		
			theReference.lastQuestionAnswered = question
			
			#save it
			theReference.save()

			# reload and return the appropriate version					
			return self.get( theReferenceId );
		except TheReference.DoesNotExist:
			raise ProcessingError(errMsg + " : TheReference with id " + str(theReferenceId) + " does not exist.")
		except Question.DoesNotExist:
			raise ProcessingError(errMsg + " : Question with id " + str(lastQuestionAnsweredId) + " does not exist.")
		except Exception:
			return None;
				
	def unassignLastQuestionAnswered( self, theReferenceId ):
		errMsg = "Failed to unassign element " + str(lastQuestionAnsweredId) + " for LastQuestionAnswered on TheReference"

		try:
			# get the TheReference from db
			theReference = self.get( theReferenceId ).first()	
			
			# assign to None for unassignment
			theReference.question = None			

			#save it
			theReference.save()

			# reload and return the appropriate version					
			return self.get( theReferenceId );
		except TheReference.DoesNotExist:
			raise ProcessingError(errMsg + " : TheReference with id " + str(theReferenceId) + " does not exist.")
		except Exception:
			return None;
		
	def addAnswers( self, theReferenceId, answersIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.AnswerDelegate import AnswerDelegate

		errMsg = "Failed to add elements " + str(answersIds) + " for Answers on TheReference"

		try:
			# get the TheReference
			theReference = self.get( theReferenceId ).first()
				
			# split on a comma with no spaces
			idList = answersIds.split(',')

			
			# iterate over ids
			for id in idList:
				# read the Answer		
				answer = AnswerDelegate().get(id).first();	
				# add the Answer
				theReference.answers.add(answer)
				
			# save it		
			theReference.save()
			
			# reload and return the appropriate version
			return self.get( theReferenceId );
		except TheReference.DoesNotExist:
			raise ProcessingError(errMsg + " : TheReference with id " + str(theReferenceId) + " does not exist.")
		except Answer.DoesNotExist:
			raise ProcessingError(errMsg + " : Answer does not exist.")
		except Exception:
			raise ProcessingError(errMsg) 
		
	def removeAnswers( self, theReferenceId, answersIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.AnswerDelegate import AnswerDelegate

		errMsg = "Failed to remove elements " + str(answersIds) + " for Answers on TheReference"

		try:
			# get the TheReference
			theReference = self.get( theReferenceId ).first()
				
			# split on a comma with no spaces
			idList = answersIds.split(',')
			
			# iterate over ids
			for id in idList:
				# read the Answer		
				answer = AnswerDelegate().get(id).first();	
				# add the Answer
				theReference.answers.remove(answer)
				
			# save it		
			theReference.save()
			
			# reload and return the appropriate version
			return self.get( theReferenceId );
		except TheReference.DoesNotExist:
			raise ProcessingError(errMsg + " : TheReference with id " + str(theReferenceId) + " does not exist.")
		except Answer.DoesNotExist:
			raise ProcessingError(errMsg + " : Answer does not exist.")
		except utils.DatabaseError:
			raise StorageWriteError()
		except Exception:
			raise GeneralError(errMsg) 
		
