from django.core import exceptions
from django.core import serializers
from django.db import models
from django.db import utils

from djangodemo.models.ReferenceEngine import ReferenceEngine
from djangodemo.models.QuestionGroup import QuestionGroup
from djangodemo.exceptions import Exceptions

 #======================================================================
# 
# Encapsulates data for model ReferenceEngine
#
# @author 
#
#======================================================================

#======================================================================
# Class ReferenceEngineDelegate Declaration
#======================================================================
class ReferenceEngineDelegate :

#======================================================================
# Function Declarations
#======================================================================

	def get(self, referenceEngineId ):
		try:	
			referenceEngine = ReferenceEngine.objects.filter(id=referenceEngineId)
			return referenceEngine.first();
		except ReferenceEngine.DoesNotExist:
			raise ProcessingError("ReferenceEngine with id " + str(referenceEngineId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 

	def createFromJson(self, referenceEngine):
		for model in serializers.deserialize("json", referenceEngine):
			model.save()
			return model;

	def create(self, referenceEngine):
		referenceEngine.save()
		return referenceEngine;

	def saveFromJson(self, referenceEngine):
		for model in serializers.deserialize("json", referenceEngine):
			model.save()
			return referenceEngine;
	
	def save(self, referenceEngine):
		referenceEngine.save()
		return referenceEngine;
	
	def delete(self, referenceEngineId ):
		errMsg = "Failed to delete ReferenceEngine from db using id " + str(referenceEngineId)
		
		try:
			referenceEngine = ReferenceEngine.objects.get(id=referenceEngineId)
			referenceEngine.delete()
			return True
		except ReferenceEngine.DoesNotExist:
			raise ProcessingError("ReferenceEngine with id " + str(referenceEngineId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 
	
	def getAll(self):
		try:
			all = ReferenceEngine.objects.all()
			return all;
		except utils.DatabaseError:
			raise StorageReadError("Failed to get all ReferenceEngine from db")
		except Exception:
			return None;
		
	def assignMainQuestionGroup( self, referenceEngineId, mainQuestionGroupId ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.QuestionGroupDelegate import QuestionGroupDelegate

		errMsg = "Failed to assign element " + str(mainQuestionGroupId) + " for MainQuestionGroup on ReferenceEngine"

		try:
			# get the ReferenceEngine from db
			referenceEngine = self.get( referenceEngineId ).first()	
			
			# get the QuestionGroup from db
			questionGroup = QuestionGroupDelegate().get(mainQuestionGroupId).first();
			
			# assign the MainQuestionGroup		
			referenceEngine.mainQuestionGroup = questionGroup
			
			#save it
			referenceEngine.save()

			# reload and return the appropriate version					
			return self.get( referenceEngineId );
		except ReferenceEngine.DoesNotExist:
			raise ProcessingError(errMsg + " : ReferenceEngine with id " + str(referenceEngineId) + " does not exist.")
		except QuestionGroup.DoesNotExist:
			raise ProcessingError(errMsg + " : QuestionGroup with id " + str(mainQuestionGroupId) + " does not exist.")
		except Exception:
			return None;
				
	def unassignMainQuestionGroup( self, referenceEngineId ):
		errMsg = "Failed to unassign element " + str(mainQuestionGroupId) + " for MainQuestionGroup on ReferenceEngine"

		try:
			# get the ReferenceEngine from db
			referenceEngine = self.get( referenceEngineId ).first()	
			
			# assign to None for unassignment
			referenceEngine.questionGroup = None			

			#save it
			referenceEngine.save()

			# reload and return the appropriate version					
			return self.get( referenceEngineId );
		except ReferenceEngine.DoesNotExist:
			raise ProcessingError(errMsg + " : ReferenceEngine with id " + str(referenceEngineId) + " does not exist.")
		except Exception:
			return None;
		
