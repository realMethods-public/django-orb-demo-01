from django.core import exceptions
from django.core import serializers
from django.db import models
from django.db import utils

from djangodemo.models.Question import Question
from djangodemo.models.TheResponse import TheResponse
from djangodemo.exceptions import Exceptions

 #======================================================================
# 
# Encapsulates data for model Question
#
# @author 
#
#======================================================================

#======================================================================
# Class QuestionDelegate Declaration
#======================================================================
class QuestionDelegate :

#======================================================================
# Function Declarations
#======================================================================

	def get(self, questionId ):
		try:	
			question = Question.objects.filter(id=questionId)
			return question.first();
		except Question.DoesNotExist:
			raise ProcessingError("Question with id " + str(questionId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 

	def createFromJson(self, question):
		for model in serializers.deserialize("json", question):
			model.save()
			return model;

	def create(self, question):
		question.save()
		return question;

	def saveFromJson(self, question):
		for model in serializers.deserialize("json", question):
			model.save()
			return question;
	
	def save(self, question):
		question.save()
		return question;
	
	def delete(self, questionId ):
		errMsg = "Failed to delete Question from db using id " + str(questionId)
		
		try:
			question = Question.objects.get(id=questionId)
			question.delete()
			return True
		except Question.DoesNotExist:
			raise ProcessingError("Question with id " + str(questionId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 
	
	def getAll(self):
		try:
			all = Question.objects.all()
			return all;
		except utils.DatabaseError:
			raise StorageReadError("Failed to get all Question from db")
		except Exception:
			return None;
		
	def addResponses( self, questionId, responsesIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.TheResponseDelegate import TheResponseDelegate

		errMsg = "Failed to add elements " + str(responsesIds) + " for Responses on Question"

		try:
			# get the Question
			question = self.get( questionId ).first()
				
			# split on a comma with no spaces
			idList = responsesIds.split(',')

			
			# iterate over ids
			for id in idList:
				# read the TheResponse		
				theResponse = TheResponseDelegate().get(id).first();	
				# add the TheResponse
				question.responses.add(theResponse)
				
			# save it		
			question.save()
			
			# reload and return the appropriate version
			return self.get( questionId );
		except Question.DoesNotExist:
			raise ProcessingError(errMsg + " : Question with id " + str(questionId) + " does not exist.")
		except TheResponse.DoesNotExist:
			raise ProcessingError(errMsg + " : TheResponse does not exist.")
		except Exception:
			raise ProcessingError(errMsg) 
		
	def removeResponses( self, questionId, responsesIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.TheResponseDelegate import TheResponseDelegate

		errMsg = "Failed to remove elements " + str(responsesIds) + " for Responses on Question"

		try:
			# get the Question
			question = self.get( questionId ).first()
				
			# split on a comma with no spaces
			idList = responsesIds.split(',')
			
			# iterate over ids
			for id in idList:
				# read the TheResponse		
				theResponse = TheResponseDelegate().get(id).first();	
				# add the TheResponse
				question.responses.remove(theResponse)
				
			# save it		
			question.save()
			
			# reload and return the appropriate version
			return self.get( questionId );
		except Question.DoesNotExist:
			raise ProcessingError(errMsg + " : Question with id " + str(questionId) + " does not exist.")
		except TheResponse.DoesNotExist:
			raise ProcessingError(errMsg + " : TheResponse does not exist.")
		except utils.DatabaseError:
			raise StorageWriteError()
		except Exception:
			raise GeneralError(errMsg) 
		
