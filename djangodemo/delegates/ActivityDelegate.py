from django.core import exceptions
from django.core import serializers
from django.db import models
from django.db import utils

from djangodemo.models.Activity import Activity
from djangodemo.models.User import User
from djangodemo.exceptions import Exceptions

 #======================================================================
# 
# Encapsulates data for model Activity
#
# @author 
#
#======================================================================

#======================================================================
# Class ActivityDelegate Declaration
#======================================================================
class ActivityDelegate :

#======================================================================
# Function Declarations
#======================================================================

	def get(self, activityId ):
		try:	
			activity = Activity.objects.filter(id=activityId)
			return activity.first();
		except Activity.DoesNotExist:
			raise ProcessingError("Activity with id " + str(activityId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 

	def createFromJson(self, activity):
		for model in serializers.deserialize("json", activity):
			model.save()
			return model;

	def create(self, activity):
		activity.save()
		return activity;

	def saveFromJson(self, activity):
		for model in serializers.deserialize("json", activity):
			model.save()
			return activity;
	
	def save(self, activity):
		activity.save()
		return activity;
	
	def delete(self, activityId ):
		errMsg = "Failed to delete Activity from db using id " + str(activityId)
		
		try:
			activity = Activity.objects.get(id=activityId)
			activity.delete()
			return True
		except Activity.DoesNotExist:
			raise ProcessingError("Activity with id " + str(activityId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 
	
	def getAll(self):
		try:
			all = Activity.objects.all()
			return all;
		except utils.DatabaseError:
			raise StorageReadError("Failed to get all Activity from db")
		except Exception:
			return None;
		
	def assignUser( self, activityId, userId ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.UserDelegate import UserDelegate

		errMsg = "Failed to assign element " + str(userId) + " for User on Activity"

		try:
			# get the Activity from db
			activity = self.get( activityId ).first()	
			
			# get the User from db
			user = UserDelegate().get(userId).first();
			
			# assign the User		
			activity.user = user
			
			#save it
			activity.save()

			# reload and return the appropriate version					
			return self.get( activityId );
		except Activity.DoesNotExist:
			raise ProcessingError(errMsg + " : Activity with id " + str(activityId) + " does not exist.")
		except User.DoesNotExist:
			raise ProcessingError(errMsg + " : User with id " + str(userId) + " does not exist.")
		except Exception:
			return None;
				
	def unassignUser( self, activityId ):
		errMsg = "Failed to unassign element " + str(userId) + " for User on Activity"

		try:
			# get the Activity from db
			activity = self.get( activityId ).first()	
			
			# assign to None for unassignment
			activity.user = None			

			#save it
			activity.save()

			# reload and return the appropriate version					
			return self.get( activityId );
		except Activity.DoesNotExist:
			raise ProcessingError(errMsg + " : Activity with id " + str(activityId) + " does not exist.")
		except Exception:
			return None;
		
