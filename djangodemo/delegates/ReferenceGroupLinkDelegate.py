from django.core import exceptions
from django.core import serializers
from django.db import models
from django.db import utils

from djangodemo.models.ReferenceGroupLink import ReferenceGroupLink
from djangodemo.models.ReferrerGroup import ReferrerGroup
from djangodemo.models.User import User
from djangodemo.exceptions import Exceptions

 #======================================================================
# 
# Encapsulates data for model ReferenceGroupLink
#
# @author 
#
#======================================================================

#======================================================================
# Class ReferenceGroupLinkDelegate Declaration
#======================================================================
class ReferenceGroupLinkDelegate :

#======================================================================
# Function Declarations
#======================================================================

	def get(self, referenceGroupLinkId ):
		try:	
			referenceGroupLink = ReferenceGroupLink.objects.filter(id=referenceGroupLinkId)
			return referenceGroupLink.first();
		except ReferenceGroupLink.DoesNotExist:
			raise ProcessingError("ReferenceGroupLink with id " + str(referenceGroupLinkId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 

	def createFromJson(self, referenceGroupLink):
		for model in serializers.deserialize("json", referenceGroupLink):
			model.save()
			return model;

	def create(self, referenceGroupLink):
		referenceGroupLink.save()
		return referenceGroupLink;

	def saveFromJson(self, referenceGroupLink):
		for model in serializers.deserialize("json", referenceGroupLink):
			model.save()
			return referenceGroupLink;
	
	def save(self, referenceGroupLink):
		referenceGroupLink.save()
		return referenceGroupLink;
	
	def delete(self, referenceGroupLinkId ):
		errMsg = "Failed to delete ReferenceGroupLink from db using id " + str(referenceGroupLinkId)
		
		try:
			referenceGroupLink = ReferenceGroupLink.objects.get(id=referenceGroupLinkId)
			referenceGroupLink.delete()
			return True
		except ReferenceGroupLink.DoesNotExist:
			raise ProcessingError("ReferenceGroupLink with id " + str(referenceGroupLinkId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 
	
	def getAll(self):
		try:
			all = ReferenceGroupLink.objects.all()
			return all;
		except utils.DatabaseError:
			raise StorageReadError("Failed to get all ReferenceGroupLink from db")
		except Exception:
			return None;
		
	def assignReferrerGroup( self, referenceGroupLinkId, referrerGroupId ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.ReferrerGroupDelegate import ReferrerGroupDelegate

		errMsg = "Failed to assign element " + str(referrerGroupId) + " for ReferrerGroup on ReferenceGroupLink"

		try:
			# get the ReferenceGroupLink from db
			referenceGroupLink = self.get( referenceGroupLinkId ).first()	
			
			# get the ReferrerGroup from db
			referrerGroup = ReferrerGroupDelegate().get(referrerGroupId).first();
			
			# assign the ReferrerGroup		
			referenceGroupLink.referrerGroup = referrerGroup
			
			#save it
			referenceGroupLink.save()

			# reload and return the appropriate version					
			return self.get( referenceGroupLinkId );
		except ReferenceGroupLink.DoesNotExist:
			raise ProcessingError(errMsg + " : ReferenceGroupLink with id " + str(referenceGroupLinkId) + " does not exist.")
		except ReferrerGroup.DoesNotExist:
			raise ProcessingError(errMsg + " : ReferrerGroup with id " + str(referrerGroupId) + " does not exist.")
		except Exception:
			return None;
				
	def unassignReferrerGroup( self, referenceGroupLinkId ):
		errMsg = "Failed to unassign element " + str(referrerGroupId) + " for ReferrerGroup on ReferenceGroupLink"

		try:
			# get the ReferenceGroupLink from db
			referenceGroupLink = self.get( referenceGroupLinkId ).first()	
			
			# assign to None for unassignment
			referenceGroupLink.referrerGroup = None			

			#save it
			referenceGroupLink.save()

			# reload and return the appropriate version					
			return self.get( referenceGroupLinkId );
		except ReferenceGroupLink.DoesNotExist:
			raise ProcessingError(errMsg + " : ReferenceGroupLink with id " + str(referenceGroupLinkId) + " does not exist.")
		except Exception:
			return None;
		
	def assignLinkProvider( self, referenceGroupLinkId, linkProviderId ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.UserDelegate import UserDelegate

		errMsg = "Failed to assign element " + str(linkProviderId) + " for LinkProvider on ReferenceGroupLink"

		try:
			# get the ReferenceGroupLink from db
			referenceGroupLink = self.get( referenceGroupLinkId ).first()	
			
			# get the User from db
			user = UserDelegate().get(linkProviderId).first();
			
			# assign the LinkProvider		
			referenceGroupLink.linkProvider = user
			
			#save it
			referenceGroupLink.save()

			# reload and return the appropriate version					
			return self.get( referenceGroupLinkId );
		except ReferenceGroupLink.DoesNotExist:
			raise ProcessingError(errMsg + " : ReferenceGroupLink with id " + str(referenceGroupLinkId) + " does not exist.")
		except User.DoesNotExist:
			raise ProcessingError(errMsg + " : User with id " + str(linkProviderId) + " does not exist.")
		except Exception:
			return None;
				
	def unassignLinkProvider( self, referenceGroupLinkId ):
		errMsg = "Failed to unassign element " + str(linkProviderId) + " for LinkProvider on ReferenceGroupLink"

		try:
			# get the ReferenceGroupLink from db
			referenceGroupLink = self.get( referenceGroupLinkId ).first()	
			
			# assign to None for unassignment
			referenceGroupLink.user = None			

			#save it
			referenceGroupLink.save()

			# reload and return the appropriate version					
			return self.get( referenceGroupLinkId );
		except ReferenceGroupLink.DoesNotExist:
			raise ProcessingError(errMsg + " : ReferenceGroupLink with id " + str(referenceGroupLinkId) + " does not exist.")
		except Exception:
			return None;
		
