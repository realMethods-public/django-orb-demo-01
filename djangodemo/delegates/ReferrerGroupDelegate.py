from django.core import exceptions
from django.core import serializers
from django.db import models
from django.db import utils

from djangodemo.models.ReferrerGroup import ReferrerGroup
from djangodemo.models.TheReference import TheReference
from djangodemo.exceptions import Exceptions

 #======================================================================
# 
# Encapsulates data for model ReferrerGroup
#
# @author 
#
#======================================================================

#======================================================================
# Class ReferrerGroupDelegate Declaration
#======================================================================
class ReferrerGroupDelegate :

#======================================================================
# Function Declarations
#======================================================================

	def get(self, referrerGroupId ):
		try:	
			referrerGroup = ReferrerGroup.objects.filter(id=referrerGroupId)
			return referrerGroup.first();
		except ReferrerGroup.DoesNotExist:
			raise ProcessingError("ReferrerGroup with id " + str(referrerGroupId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 

	def createFromJson(self, referrerGroup):
		for model in serializers.deserialize("json", referrerGroup):
			model.save()
			return model;

	def create(self, referrerGroup):
		referrerGroup.save()
		return referrerGroup;

	def saveFromJson(self, referrerGroup):
		for model in serializers.deserialize("json", referrerGroup):
			model.save()
			return referrerGroup;
	
	def save(self, referrerGroup):
		referrerGroup.save()
		return referrerGroup;
	
	def delete(self, referrerGroupId ):
		errMsg = "Failed to delete ReferrerGroup from db using id " + str(referrerGroupId)
		
		try:
			referrerGroup = ReferrerGroup.objects.get(id=referrerGroupId)
			referrerGroup.delete()
			return True
		except ReferrerGroup.DoesNotExist:
			raise ProcessingError("ReferrerGroup with id " + str(referrerGroupId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 
	
	def getAll(self):
		try:
			all = ReferrerGroup.objects.all()
			return all;
		except utils.DatabaseError:
			raise StorageReadError("Failed to get all ReferrerGroup from db")
		except Exception:
			return None;
		
	def addReferences( self, referrerGroupId, referencesIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.TheReferenceDelegate import TheReferenceDelegate

		errMsg = "Failed to add elements " + str(referencesIds) + " for References on ReferrerGroup"

		try:
			# get the ReferrerGroup
			referrerGroup = self.get( referrerGroupId ).first()
				
			# split on a comma with no spaces
			idList = referencesIds.split(',')

			
			# iterate over ids
			for id in idList:
				# read the TheReference		
				theReference = TheReferenceDelegate().get(id).first();	
				# add the TheReference
				referrerGroup.references.add(theReference)
				
			# save it		
			referrerGroup.save()
			
			# reload and return the appropriate version
			return self.get( referrerGroupId );
		except ReferrerGroup.DoesNotExist:
			raise ProcessingError(errMsg + " : ReferrerGroup with id " + str(referrerGroupId) + " does not exist.")
		except TheReference.DoesNotExist:
			raise ProcessingError(errMsg + " : TheReference does not exist.")
		except Exception:
			raise ProcessingError(errMsg) 
		
	def removeReferences( self, referrerGroupId, referencesIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.TheReferenceDelegate import TheReferenceDelegate

		errMsg = "Failed to remove elements " + str(referencesIds) + " for References on ReferrerGroup"

		try:
			# get the ReferrerGroup
			referrerGroup = self.get( referrerGroupId ).first()
				
			# split on a comma with no spaces
			idList = referencesIds.split(',')
			
			# iterate over ids
			for id in idList:
				# read the TheReference		
				theReference = TheReferenceDelegate().get(id).first();	
				# add the TheReference
				referrerGroup.references.remove(theReference)
				
			# save it		
			referrerGroup.save()
			
			# reload and return the appropriate version
			return self.get( referrerGroupId );
		except ReferrerGroup.DoesNotExist:
			raise ProcessingError(errMsg + " : ReferrerGroup with id " + str(referrerGroupId) + " does not exist.")
		except TheReference.DoesNotExist:
			raise ProcessingError(errMsg + " : TheReference does not exist.")
		except utils.DatabaseError:
			raise StorageWriteError()
		except Exception:
			raise GeneralError(errMsg) 
		
