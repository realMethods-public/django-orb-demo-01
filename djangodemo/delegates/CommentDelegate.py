from django.core import exceptions
from django.core import serializers
from django.db import models
from django.db import utils

from djangodemo.models.Comment import Comment
from djangodemo.models.Referrer import Referrer
from djangodemo.exceptions import Exceptions

 #======================================================================
# 
# Encapsulates data for model Comment
#
# @author 
#
#======================================================================

#======================================================================
# Class CommentDelegate Declaration
#======================================================================
class CommentDelegate :

#======================================================================
# Function Declarations
#======================================================================

	def get(self, commentId ):
		try:	
			comment = Comment.objects.filter(id=commentId)
			return comment.first();
		except Comment.DoesNotExist:
			raise ProcessingError("Comment with id " + str(commentId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 

	def createFromJson(self, comment):
		for model in serializers.deserialize("json", comment):
			model.save()
			return model;

	def create(self, comment):
		comment.save()
		return comment;

	def saveFromJson(self, comment):
		for model in serializers.deserialize("json", comment):
			model.save()
			return comment;
	
	def save(self, comment):
		comment.save()
		return comment;
	
	def delete(self, commentId ):
		errMsg = "Failed to delete Comment from db using id " + str(commentId)
		
		try:
			comment = Comment.objects.get(id=commentId)
			comment.delete()
			return True
		except Comment.DoesNotExist:
			raise ProcessingError("Comment with id " + str(commentId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 
	
	def getAll(self):
		try:
			all = Comment.objects.all()
			return all;
		except utils.DatabaseError:
			raise StorageReadError("Failed to get all Comment from db")
		except Exception:
			return None;
		
	def assignSource( self, commentId, sourceId ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.ReferrerDelegate import ReferrerDelegate

		errMsg = "Failed to assign element " + str(sourceId) + " for Source on Comment"

		try:
			# get the Comment from db
			comment = self.get( commentId ).first()	
			
			# get the Referrer from db
			referrer = ReferrerDelegate().get(sourceId).first();
			
			# assign the Source		
			comment.source = referrer
			
			#save it
			comment.save()

			# reload and return the appropriate version					
			return self.get( commentId );
		except Comment.DoesNotExist:
			raise ProcessingError(errMsg + " : Comment with id " + str(commentId) + " does not exist.")
		except Referrer.DoesNotExist:
			raise ProcessingError(errMsg + " : Referrer with id " + str(sourceId) + " does not exist.")
		except Exception:
			return None;
				
	def unassignSource( self, commentId ):
		errMsg = "Failed to unassign element " + str(sourceId) + " for Source on Comment"

		try:
			# get the Comment from db
			comment = self.get( commentId ).first()	
			
			# assign to None for unassignment
			comment.referrer = None			

			#save it
			comment.save()

			# reload and return the appropriate version					
			return self.get( commentId );
		except Comment.DoesNotExist:
			raise ProcessingError(errMsg + " : Comment with id " + str(commentId) + " does not exist.")
		except Exception:
			return None;
		
