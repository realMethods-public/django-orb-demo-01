from django.core import exceptions
from django.core import serializers
from django.db import models
from django.db import utils

from djangodemo.models.Admin import Admin
from djangodemo.models.User import User
from djangodemo.models.ReferenceEngine import ReferenceEngine
from djangodemo.exceptions import Exceptions

 #======================================================================
# 
# Encapsulates data for model Admin
#
# @author 
#
#======================================================================

#======================================================================
# Class AdminDelegate Declaration
#======================================================================
class AdminDelegate :

#======================================================================
# Function Declarations
#======================================================================

	def get(self, adminId ):
		try:	
			admin = Admin.objects.filter(id=adminId)
			return admin.first();
		except Admin.DoesNotExist:
			raise ProcessingError("Admin with id " + str(adminId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 

	def createFromJson(self, admin):
		for model in serializers.deserialize("json", admin):
			model.save()
			return model;

	def create(self, admin):
		admin.save()
		return admin;

	def saveFromJson(self, admin):
		for model in serializers.deserialize("json", admin):
			model.save()
			return admin;
	
	def save(self, admin):
		admin.save()
		return admin;
	
	def delete(self, adminId ):
		errMsg = "Failed to delete Admin from db using id " + str(adminId)
		
		try:
			admin = Admin.objects.get(id=adminId)
			admin.delete()
			return True
		except Admin.DoesNotExist:
			raise ProcessingError("Admin with id " + str(adminId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 
	
	def getAll(self):
		try:
			all = Admin.objects.all()
			return all;
		except utils.DatabaseError:
			raise StorageReadError("Failed to get all Admin from db")
		except Exception:
			return None;
		
	def addUsers( self, adminId, usersIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.UserDelegate import UserDelegate

		errMsg = "Failed to add elements " + str(usersIds) + " for Users on Admin"

		try:
			# get the Admin
			admin = self.get( adminId ).first()
				
			# split on a comma with no spaces
			idList = usersIds.split(',')

			
			# iterate over ids
			for id in idList:
				# read the User		
				user = UserDelegate().get(id).first();	
				# add the User
				admin.users.add(user)
				
			# save it		
			admin.save()
			
			# reload and return the appropriate version
			return self.get( adminId );
		except Admin.DoesNotExist:
			raise ProcessingError(errMsg + " : Admin with id " + str(adminId) + " does not exist.")
		except User.DoesNotExist:
			raise ProcessingError(errMsg + " : User does not exist.")
		except Exception:
			raise ProcessingError(errMsg) 
		
	def removeUsers( self, adminId, usersIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.UserDelegate import UserDelegate

		errMsg = "Failed to remove elements " + str(usersIds) + " for Users on Admin"

		try:
			# get the Admin
			admin = self.get( adminId ).first()
				
			# split on a comma with no spaces
			idList = usersIds.split(',')
			
			# iterate over ids
			for id in idList:
				# read the User		
				user = UserDelegate().get(id).first();	
				# add the User
				admin.users.remove(user)
				
			# save it		
			admin.save()
			
			# reload and return the appropriate version
			return self.get( adminId );
		except Admin.DoesNotExist:
			raise ProcessingError(errMsg + " : Admin with id " + str(adminId) + " does not exist.")
		except User.DoesNotExist:
			raise ProcessingError(errMsg + " : User does not exist.")
		except utils.DatabaseError:
			raise StorageWriteError()
		except Exception:
			raise GeneralError(errMsg) 
		
	def addReferenceEngines( self, adminId, referenceEnginesIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.ReferenceEngineDelegate import ReferenceEngineDelegate

		errMsg = "Failed to add elements " + str(referenceEnginesIds) + " for ReferenceEngines on Admin"

		try:
			# get the Admin
			admin = self.get( adminId ).first()
				
			# split on a comma with no spaces
			idList = referenceEnginesIds.split(',')

			
			# iterate over ids
			for id in idList:
				# read the ReferenceEngine		
				referenceEngine = ReferenceEngineDelegate().get(id).first();	
				# add the ReferenceEngine
				admin.referenceEngines.add(referenceEngine)
				
			# save it		
			admin.save()
			
			# reload and return the appropriate version
			return self.get( adminId );
		except Admin.DoesNotExist:
			raise ProcessingError(errMsg + " : Admin with id " + str(adminId) + " does not exist.")
		except ReferenceEngine.DoesNotExist:
			raise ProcessingError(errMsg + " : ReferenceEngine does not exist.")
		except Exception:
			raise ProcessingError(errMsg) 
		
	def removeReferenceEngines( self, adminId, referenceEnginesIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.ReferenceEngineDelegate import ReferenceEngineDelegate

		errMsg = "Failed to remove elements " + str(referenceEnginesIds) + " for ReferenceEngines on Admin"

		try:
			# get the Admin
			admin = self.get( adminId ).first()
				
			# split on a comma with no spaces
			idList = referenceEnginesIds.split(',')
			
			# iterate over ids
			for id in idList:
				# read the ReferenceEngine		
				referenceEngine = ReferenceEngineDelegate().get(id).first();	
				# add the ReferenceEngine
				admin.referenceEngines.remove(referenceEngine)
				
			# save it		
			admin.save()
			
			# reload and return the appropriate version
			return self.get( adminId );
		except Admin.DoesNotExist:
			raise ProcessingError(errMsg + " : Admin with id " + str(adminId) + " does not exist.")
		except ReferenceEngine.DoesNotExist:
			raise ProcessingError(errMsg + " : ReferenceEngine does not exist.")
		except utils.DatabaseError:
			raise StorageWriteError()
		except Exception:
			raise GeneralError(errMsg) 
		
