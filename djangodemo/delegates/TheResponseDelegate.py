from django.core import exceptions
from django.core import serializers
from django.db import models
from django.db import utils

from djangodemo.models.TheResponse import TheResponse
from djangodemo.exceptions import Exceptions

 #======================================================================
# 
# Encapsulates data for model TheResponse
#
# @author 
#
#======================================================================

#======================================================================
# Class TheResponseDelegate Declaration
#======================================================================
class TheResponseDelegate :

#======================================================================
# Function Declarations
#======================================================================

	def get(self, theResponseId ):
		try:	
			theResponse = TheResponse.objects.filter(id=theResponseId)
			return theResponse.first();
		except TheResponse.DoesNotExist:
			raise ProcessingError("TheResponse with id " + str(theResponseId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 

	def createFromJson(self, theResponse):
		for model in serializers.deserialize("json", theResponse):
			model.save()
			return model;

	def create(self, theResponse):
		theResponse.save()
		return theResponse;

	def saveFromJson(self, theResponse):
		for model in serializers.deserialize("json", theResponse):
			model.save()
			return theResponse;
	
	def save(self, theResponse):
		theResponse.save()
		return theResponse;
	
	def delete(self, theResponseId ):
		errMsg = "Failed to delete TheResponse from db using id " + str(theResponseId)
		
		try:
			theResponse = TheResponse.objects.get(id=theResponseId)
			theResponse.delete()
			return True
		except TheResponse.DoesNotExist:
			raise ProcessingError("TheResponse with id " + str(theResponseId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 
	
	def getAll(self):
		try:
			all = TheResponse.objects.all()
			return all;
		except utils.DatabaseError:
			raise StorageReadError("Failed to get all TheResponse from db")
		except Exception:
			return None;
		
