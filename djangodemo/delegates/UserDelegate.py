from django.core import exceptions
from django.core import serializers
from django.db import models
from django.db import utils

from djangodemo.models.User import User
from djangodemo.models.Referrer import Referrer
from djangodemo.models.ReferrerGroup import ReferrerGroup
from djangodemo.models.ReferenceGroupLink import ReferenceGroupLink
from djangodemo.exceptions import Exceptions

 #======================================================================
# 
# Encapsulates data for model User
#
# @author 
#
#======================================================================

#======================================================================
# Class UserDelegate Declaration
#======================================================================
class UserDelegate :

#======================================================================
# Function Declarations
#======================================================================

	def get(self, userId ):
		try:	
			user = User.objects.filter(id=userId)
			return user.first();
		except User.DoesNotExist:
			raise ProcessingError("User with id " + str(userId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 

	def createFromJson(self, user):
		for model in serializers.deserialize("json", user):
			model.save()
			return model;

	def create(self, user):
		user.save()
		return user;

	def saveFromJson(self, user):
		for model in serializers.deserialize("json", user):
			model.save()
			return user;
	
	def save(self, user):
		user.save()
		return user;
	
	def delete(self, userId ):
		errMsg = "Failed to delete User from db using id " + str(userId)
		
		try:
			user = User.objects.get(id=userId)
			user.delete()
			return True
		except User.DoesNotExist:
			raise ProcessingError("User with id " + str(userId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 
	
	def getAll(self):
		try:
			all = User.objects.all()
			return all;
		except utils.DatabaseError:
			raise StorageReadError("Failed to get all User from db")
		except Exception:
			return None;
		
	def addReferenceProviders( self, userId, referenceProvidersIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.ReferrerDelegate import ReferrerDelegate

		errMsg = "Failed to add elements " + str(referenceProvidersIds) + " for ReferenceProviders on User"

		try:
			# get the User
			user = self.get( userId ).first()
				
			# split on a comma with no spaces
			idList = referenceProvidersIds.split(',')

			
			# iterate over ids
			for id in idList:
				# read the Referrer		
				referrer = ReferrerDelegate().get(id).first();	
				# add the Referrer
				user.referenceProviders.add(referrer)
				
			# save it		
			user.save()
			
			# reload and return the appropriate version
			return self.get( userId );
		except User.DoesNotExist:
			raise ProcessingError(errMsg + " : User with id " + str(userId) + " does not exist.")
		except Referrer.DoesNotExist:
			raise ProcessingError(errMsg + " : Referrer does not exist.")
		except Exception:
			raise ProcessingError(errMsg) 
		
	def removeReferenceProviders( self, userId, referenceProvidersIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.ReferrerDelegate import ReferrerDelegate

		errMsg = "Failed to remove elements " + str(referenceProvidersIds) + " for ReferenceProviders on User"

		try:
			# get the User
			user = self.get( userId ).first()
				
			# split on a comma with no spaces
			idList = referenceProvidersIds.split(',')
			
			# iterate over ids
			for id in idList:
				# read the Referrer		
				referrer = ReferrerDelegate().get(id).first();	
				# add the Referrer
				user.referenceProviders.remove(referrer)
				
			# save it		
			user.save()
			
			# reload and return the appropriate version
			return self.get( userId );
		except User.DoesNotExist:
			raise ProcessingError(errMsg + " : User with id " + str(userId) + " does not exist.")
		except Referrer.DoesNotExist:
			raise ProcessingError(errMsg + " : Referrer does not exist.")
		except utils.DatabaseError:
			raise StorageWriteError()
		except Exception:
			raise GeneralError(errMsg) 
		
	def addRefererGroups( self, userId, refererGroupsIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.ReferrerGroupDelegate import ReferrerGroupDelegate

		errMsg = "Failed to add elements " + str(refererGroupsIds) + " for RefererGroups on User"

		try:
			# get the User
			user = self.get( userId ).first()
				
			# split on a comma with no spaces
			idList = refererGroupsIds.split(',')

			
			# iterate over ids
			for id in idList:
				# read the ReferrerGroup		
				referrerGroup = ReferrerGroupDelegate().get(id).first();	
				# add the ReferrerGroup
				user.refererGroups.add(referrerGroup)
				
			# save it		
			user.save()
			
			# reload and return the appropriate version
			return self.get( userId );
		except User.DoesNotExist:
			raise ProcessingError(errMsg + " : User with id " + str(userId) + " does not exist.")
		except ReferrerGroup.DoesNotExist:
			raise ProcessingError(errMsg + " : ReferrerGroup does not exist.")
		except Exception:
			raise ProcessingError(errMsg) 
		
	def removeRefererGroups( self, userId, refererGroupsIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.ReferrerGroupDelegate import ReferrerGroupDelegate

		errMsg = "Failed to remove elements " + str(refererGroupsIds) + " for RefererGroups on User"

		try:
			# get the User
			user = self.get( userId ).first()
				
			# split on a comma with no spaces
			idList = refererGroupsIds.split(',')
			
			# iterate over ids
			for id in idList:
				# read the ReferrerGroup		
				referrerGroup = ReferrerGroupDelegate().get(id).first();	
				# add the ReferrerGroup
				user.refererGroups.remove(referrerGroup)
				
			# save it		
			user.save()
			
			# reload and return the appropriate version
			return self.get( userId );
		except User.DoesNotExist:
			raise ProcessingError(errMsg + " : User with id " + str(userId) + " does not exist.")
		except ReferrerGroup.DoesNotExist:
			raise ProcessingError(errMsg + " : ReferrerGroup does not exist.")
		except utils.DatabaseError:
			raise StorageWriteError()
		except Exception:
			raise GeneralError(errMsg) 
		
	def addReferenceReceivers( self, userId, referenceReceiversIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.ReferrerDelegate import ReferrerDelegate

		errMsg = "Failed to add elements " + str(referenceReceiversIds) + " for ReferenceReceivers on User"

		try:
			# get the User
			user = self.get( userId ).first()
				
			# split on a comma with no spaces
			idList = referenceReceiversIds.split(',')

			
			# iterate over ids
			for id in idList:
				# read the Referrer		
				referrer = ReferrerDelegate().get(id).first();	
				# add the Referrer
				user.referenceReceivers.add(referrer)
				
			# save it		
			user.save()
			
			# reload and return the appropriate version
			return self.get( userId );
		except User.DoesNotExist:
			raise ProcessingError(errMsg + " : User with id " + str(userId) + " does not exist.")
		except Referrer.DoesNotExist:
			raise ProcessingError(errMsg + " : Referrer does not exist.")
		except Exception:
			raise ProcessingError(errMsg) 
		
	def removeReferenceReceivers( self, userId, referenceReceiversIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.ReferrerDelegate import ReferrerDelegate

		errMsg = "Failed to remove elements " + str(referenceReceiversIds) + " for ReferenceReceivers on User"

		try:
			# get the User
			user = self.get( userId ).first()
				
			# split on a comma with no spaces
			idList = referenceReceiversIds.split(',')
			
			# iterate over ids
			for id in idList:
				# read the Referrer		
				referrer = ReferrerDelegate().get(id).first();	
				# add the Referrer
				user.referenceReceivers.remove(referrer)
				
			# save it		
			user.save()
			
			# reload and return the appropriate version
			return self.get( userId );
		except User.DoesNotExist:
			raise ProcessingError(errMsg + " : User with id " + str(userId) + " does not exist.")
		except Referrer.DoesNotExist:
			raise ProcessingError(errMsg + " : Referrer does not exist.")
		except utils.DatabaseError:
			raise StorageWriteError()
		except Exception:
			raise GeneralError(errMsg) 
		
	def addReferenceGroupLinks( self, userId, referenceGroupLinksIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.ReferenceGroupLinkDelegate import ReferenceGroupLinkDelegate

		errMsg = "Failed to add elements " + str(referenceGroupLinksIds) + " for ReferenceGroupLinks on User"

		try:
			# get the User
			user = self.get( userId ).first()
				
			# split on a comma with no spaces
			idList = referenceGroupLinksIds.split(',')

			
			# iterate over ids
			for id in idList:
				# read the ReferenceGroupLink		
				referenceGroupLink = ReferenceGroupLinkDelegate().get(id).first();	
				# add the ReferenceGroupLink
				user.referenceGroupLinks.add(referenceGroupLink)
				
			# save it		
			user.save()
			
			# reload and return the appropriate version
			return self.get( userId );
		except User.DoesNotExist:
			raise ProcessingError(errMsg + " : User with id " + str(userId) + " does not exist.")
		except ReferenceGroupLink.DoesNotExist:
			raise ProcessingError(errMsg + " : ReferenceGroupLink does not exist.")
		except Exception:
			raise ProcessingError(errMsg) 
		
	def removeReferenceGroupLinks( self, userId, referenceGroupLinksIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.ReferenceGroupLinkDelegate import ReferenceGroupLinkDelegate

		errMsg = "Failed to remove elements " + str(referenceGroupLinksIds) + " for ReferenceGroupLinks on User"

		try:
			# get the User
			user = self.get( userId ).first()
				
			# split on a comma with no spaces
			idList = referenceGroupLinksIds.split(',')
			
			# iterate over ids
			for id in idList:
				# read the ReferenceGroupLink		
				referenceGroupLink = ReferenceGroupLinkDelegate().get(id).first();	
				# add the ReferenceGroupLink
				user.referenceGroupLinks.remove(referenceGroupLink)
				
			# save it		
			user.save()
			
			# reload and return the appropriate version
			return self.get( userId );
		except User.DoesNotExist:
			raise ProcessingError(errMsg + " : User with id " + str(userId) + " does not exist.")
		except ReferenceGroupLink.DoesNotExist:
			raise ProcessingError(errMsg + " : ReferenceGroupLink does not exist.")
		except utils.DatabaseError:
			raise StorageWriteError()
		except Exception:
			raise GeneralError(errMsg) 
		
