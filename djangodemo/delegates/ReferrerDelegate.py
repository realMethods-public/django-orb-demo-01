from django.core import exceptions
from django.core import serializers
from django.db import models
from django.db import utils

from djangodemo.models.Referrer import Referrer
from djangodemo.models.Comment import Comment
from djangodemo.exceptions import Exceptions

 #======================================================================
# 
# Encapsulates data for model Referrer
#
# @author 
#
#======================================================================

#======================================================================
# Class ReferrerDelegate Declaration
#======================================================================
class ReferrerDelegate :

#======================================================================
# Function Declarations
#======================================================================

	def get(self, referrerId ):
		try:	
			referrer = Referrer.objects.filter(id=referrerId)
			return referrer.first();
		except Referrer.DoesNotExist:
			raise ProcessingError("Referrer with id " + str(referrerId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 

	def createFromJson(self, referrer):
		for model in serializers.deserialize("json", referrer):
			model.save()
			return model;

	def create(self, referrer):
		referrer.save()
		return referrer;

	def saveFromJson(self, referrer):
		for model in serializers.deserialize("json", referrer):
			model.save()
			return referrer;
	
	def save(self, referrer):
		referrer.save()
		return referrer;
	
	def delete(self, referrerId ):
		errMsg = "Failed to delete Referrer from db using id " + str(referrerId)
		
		try:
			referrer = Referrer.objects.get(id=referrerId)
			referrer.delete()
			return True
		except Referrer.DoesNotExist:
			raise ProcessingError("Referrer with id " + str(referrerId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 
	
	def getAll(self):
		try:
			all = Referrer.objects.all()
			return all;
		except utils.DatabaseError:
			raise StorageReadError("Failed to get all Referrer from db")
		except Exception:
			return None;
		
	def addComments( self, referrerId, commentsIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.CommentDelegate import CommentDelegate

		errMsg = "Failed to add elements " + str(commentsIds) + " for Comments on Referrer"

		try:
			# get the Referrer
			referrer = self.get( referrerId ).first()
				
			# split on a comma with no spaces
			idList = commentsIds.split(',')

			
			# iterate over ids
			for id in idList:
				# read the Comment		
				comment = CommentDelegate().get(id).first();	
				# add the Comment
				referrer.comments.add(comment)
				
			# save it		
			referrer.save()
			
			# reload and return the appropriate version
			return self.get( referrerId );
		except Referrer.DoesNotExist:
			raise ProcessingError(errMsg + " : Referrer with id " + str(referrerId) + " does not exist.")
		except Comment.DoesNotExist:
			raise ProcessingError(errMsg + " : Comment does not exist.")
		except Exception:
			raise ProcessingError(errMsg) 
		
	def removeComments( self, referrerId, commentsIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.CommentDelegate import CommentDelegate

		errMsg = "Failed to remove elements " + str(commentsIds) + " for Comments on Referrer"

		try:
			# get the Referrer
			referrer = self.get( referrerId ).first()
				
			# split on a comma with no spaces
			idList = commentsIds.split(',')
			
			# iterate over ids
			for id in idList:
				# read the Comment		
				comment = CommentDelegate().get(id).first();	
				# add the Comment
				referrer.comments.remove(comment)
				
			# save it		
			referrer.save()
			
			# reload and return the appropriate version
			return self.get( referrerId );
		except Referrer.DoesNotExist:
			raise ProcessingError(errMsg + " : Referrer with id " + str(referrerId) + " does not exist.")
		except Comment.DoesNotExist:
			raise ProcessingError(errMsg + " : Comment does not exist.")
		except utils.DatabaseError:
			raise StorageWriteError()
		except Exception:
			raise GeneralError(errMsg) 
		
