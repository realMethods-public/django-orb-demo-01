from django.core import exceptions
from django.core import serializers
from django.db import models
from django.db import utils

from djangodemo.models.QuestionGroup import QuestionGroup
from djangodemo.models.Question import Question
from djangodemo.exceptions import Exceptions

 #======================================================================
# 
# Encapsulates data for model QuestionGroup
#
# @author 
#
#======================================================================

#======================================================================
# Class QuestionGroupDelegate Declaration
#======================================================================
class QuestionGroupDelegate :

#======================================================================
# Function Declarations
#======================================================================

	def get(self, questionGroupId ):
		try:	
			questionGroup = QuestionGroup.objects.filter(id=questionGroupId)
			return questionGroup.first();
		except QuestionGroup.DoesNotExist:
			raise ProcessingError("QuestionGroup with id " + str(questionGroupId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 

	def createFromJson(self, questionGroup):
		for model in serializers.deserialize("json", questionGroup):
			model.save()
			return model;

	def create(self, questionGroup):
		questionGroup.save()
		return questionGroup;

	def saveFromJson(self, questionGroup):
		for model in serializers.deserialize("json", questionGroup):
			model.save()
			return questionGroup;
	
	def save(self, questionGroup):
		questionGroup.save()
		return questionGroup;
	
	def delete(self, questionGroupId ):
		errMsg = "Failed to delete QuestionGroup from db using id " + str(questionGroupId)
		
		try:
			questionGroup = QuestionGroup.objects.get(id=questionGroupId)
			questionGroup.delete()
			return True
		except QuestionGroup.DoesNotExist:
			raise ProcessingError("QuestionGroup with id " + str(questionGroupId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 
	
	def getAll(self):
		try:
			all = QuestionGroup.objects.all()
			return all;
		except utils.DatabaseError:
			raise StorageReadError("Failed to get all QuestionGroup from db")
		except Exception:
			return None;
		
	def addQuestions( self, questionGroupId, questionsIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.QuestionDelegate import QuestionDelegate

		errMsg = "Failed to add elements " + str(questionsIds) + " for Questions on QuestionGroup"

		try:
			# get the QuestionGroup
			questionGroup = self.get( questionGroupId ).first()
				
			# split on a comma with no spaces
			idList = questionsIds.split(',')

			
			# iterate over ids
			for id in idList:
				# read the Question		
				question = QuestionDelegate().get(id).first();	
				# add the Question
				questionGroup.questions.add(question)
				
			# save it		
			questionGroup.save()
			
			# reload and return the appropriate version
			return self.get( questionGroupId );
		except QuestionGroup.DoesNotExist:
			raise ProcessingError(errMsg + " : QuestionGroup with id " + str(questionGroupId) + " does not exist.")
		except Question.DoesNotExist:
			raise ProcessingError(errMsg + " : Question does not exist.")
		except Exception:
			raise ProcessingError(errMsg) 
		
	def removeQuestions( self, questionGroupId, questionsIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.QuestionDelegate import QuestionDelegate

		errMsg = "Failed to remove elements " + str(questionsIds) + " for Questions on QuestionGroup"

		try:
			# get the QuestionGroup
			questionGroup = self.get( questionGroupId ).first()
				
			# split on a comma with no spaces
			idList = questionsIds.split(',')
			
			# iterate over ids
			for id in idList:
				# read the Question		
				question = QuestionDelegate().get(id).first();	
				# add the Question
				questionGroup.questions.remove(question)
				
			# save it		
			questionGroup.save()
			
			# reload and return the appropriate version
			return self.get( questionGroupId );
		except QuestionGroup.DoesNotExist:
			raise ProcessingError(errMsg + " : QuestionGroup with id " + str(questionGroupId) + " does not exist.")
		except Question.DoesNotExist:
			raise ProcessingError(errMsg + " : Question does not exist.")
		except utils.DatabaseError:
			raise StorageWriteError()
		except Exception:
			raise GeneralError(errMsg) 
		
	def addQuestionGroups( self, questionGroupId, questionGroupsIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.QuestionGroupDelegate import QuestionGroupDelegate

		errMsg = "Failed to add elements " + str(questionGroupsIds) + " for QuestionGroups on QuestionGroup"

		try:
			# get the QuestionGroup
			questionGroup = self.get( questionGroupId ).first()
				
			# split on a comma with no spaces
			idList = questionGroupsIds.split(',')

			
			# iterate over ids
			for id in idList:
				# read the QuestionGroup		
				questionGroup = QuestionGroupDelegate().get(id).first();	
				# add the QuestionGroup
				questionGroup.questionGroups.add(questionGroup)
				
			# save it		
			questionGroup.save()
			
			# reload and return the appropriate version
			return self.get( questionGroupId );
		except QuestionGroup.DoesNotExist:
			raise ProcessingError(errMsg + " : QuestionGroup with id " + str(questionGroupId) + " does not exist.")
		except QuestionGroup.DoesNotExist:
			raise ProcessingError(errMsg + " : QuestionGroup does not exist.")
		except Exception:
			raise ProcessingError(errMsg) 
		
	def removeQuestionGroups( self, questionGroupId, questionGroupsIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.QuestionGroupDelegate import QuestionGroupDelegate

		errMsg = "Failed to remove elements " + str(questionGroupsIds) + " for QuestionGroups on QuestionGroup"

		try:
			# get the QuestionGroup
			questionGroup = self.get( questionGroupId ).first()
				
			# split on a comma with no spaces
			idList = questionGroupsIds.split(',')
			
			# iterate over ids
			for id in idList:
				# read the QuestionGroup		
				questionGroup = QuestionGroupDelegate().get(id).first();	
				# add the QuestionGroup
				questionGroup.questionGroups.remove(questionGroup)
				
			# save it		
			questionGroup.save()
			
			# reload and return the appropriate version
			return self.get( questionGroupId );
		except QuestionGroup.DoesNotExist:
			raise ProcessingError(errMsg + " : QuestionGroup with id " + str(questionGroupId) + " does not exist.")
		except QuestionGroup.DoesNotExist:
			raise ProcessingError(errMsg + " : QuestionGroup does not exist.")
		except utils.DatabaseError:
			raise StorageWriteError()
		except Exception:
			raise GeneralError(errMsg) 
		
